/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "image_8bits.h"


Image8_t *
Image8_new()
{
  Image8_t *p = NULL;

  p = xcalloc(1, sizeof(Image8_t));

  p->id = -1;
  p->buff = Buffer8_new();

  return p;
}


void
Image8_delete(Image8_t *p)
{
  xfree(p->name);
  xfree(p->dname);

  if (p->buff) {
    Buffer8_delete(p->buff);
  }
  xfree(p);
}


void
Image8_copy(const Image8_t *from, Image8_t *to)
{
  assert(NULL != from);
  assert(NULL != to);

  assert(NULL != from->name);

  xfree(to->name);

  to->name = strdup(from->name);

  to->id = from->id;

  Buffer8_copy(from->buff, to->buff);
}
