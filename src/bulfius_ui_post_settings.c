/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "settings.h"


int
callback_ui_post_settings(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;
  assert(NULL != ctx);
  const char *favorite = u_map_get(request->map_url, "favorite");

  if (NULL != favorite) {
    json_t *res = json_pack("{sssb}", "name", favorite, "favorite", Settings_switch_favorite(favorite));

    ulfius_set_json_body_response(response, 200, res);
    json_decref(res);
    ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");
  } else {
    json_error_t jerror;
    json_t *body = ulfius_get_json_body_request(request, &jerror);

    if (NULL == body) {
      fprintf(stderr, "[!] %s: JSON error: %s\n", __FILE__, jerror.text);
      ulfius_set_string_body_response(response, 400, NULL);
    } else {
      Settings_lock();
      json_t *res = json_pack("{sb}", "restart", Settings_parse_post(ctx, body));
      Settings_unlock();
      json_decref(body);
      ulfius_set_json_body_response(response, 200, res);
      json_decref(res);
      ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");
    }
  }

  return U_CALLBACK_COMPLETE;
}
