/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_SEQUENCES_H
#define __BINIOU_SEQUENCES_H

#include "shuffler.h"
#include "sequence.h"


typedef struct Sequences_s {
  GList      *seqs;
  uint16_t    size;
  Shuffler_t *shuffler;
} Sequences_t;


void Sequences_new();
void Sequences_delete();

GList *Sequences_find(const uint64_t);
GList *Sequences_find_by_name(const char *);
GList *Sequences_find_last_updated(GList *);

void Sequences_set_dir(gchar *);
const gchar *Sequences_get_dir();

void Sequences_remove_sequence_by_name(const char *);

#endif /* __BINIOU_SEQUENCES_H */
