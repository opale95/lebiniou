/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"


int
callback_ui_post_command(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;
  const char *cmd = u_map_get(request->map_url, "cmd");
  assert(NULL != cmd);
  const int command = str2command(cmd);

  if (command != -1) {
    if (is_allowed(command)) {
      json_error_t error;
      json_t *body = ulfius_get_json_body_request(request, &error);

      if (NULL != body) {
        json_t *res = Context_process_ui_command(ctx, (const enum Command)command, body, BC_WEB);

        if (NULL == res) {
          json_t *json = json_pack("{ssss}", "uiCommand", cmd, "result", "ok");
          ulfius_set_json_body_response(response, 200, json);
          bulfius_websocket_broadcast_json_message_all(ctx, json);
          json_decref(json);
        } else {
          ulfius_set_json_body_response(response, 200, res);
          json_t *json = json_pack("{ssso}", "uiCommand", cmd, "result", res);
          bulfius_websocket_broadcast_json_message_all(ctx, json);
          json_decref(json);
        }
        ulfius_add_header_to_response(response, "Content-type", "application/json");
      } else {
        fprintf(stderr, "[!] %s: JSON error: %s\n", __FILE__, error.text);
        ulfius_set_string_body_response(response, 400, "No body");
      }
      json_decref(body);
    } else {
      ulfius_set_string_body_response(response, 400, "Command not allowed");
    }
  } else {
    ulfius_set_string_body_response(response, 400, "Command not found");
  }

  return U_CALLBACK_COMPLETE;
}
