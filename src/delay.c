/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "globals.h"
#include "images.h"
#include "colormaps.h"


/* Auto-change timers and modes */
char *delay_names[MAX_TIMERS] = {
  "colormaps",
  "images",
  "sequences"
#ifdef WITH_WEBCAM
  , "webcams"
#endif
};


int delays[MAX_TIMERS][2] = {
  { DELAY_MIN, DELAY_MAX },  // colormaps
  { DELAY_MIN, DELAY_MAX },  // images
  { DELAY_MIN, DELAY_MAX }   // sequences
#ifdef WITH_WEBCAM
  , { DELAY_MIN, DELAY_MAX } // webcams
#endif
};


void
Context_create_timers(Context_t *ctx)
{
  /* Create images fader and timer */
  if (NULL != images) {
    VERBOSE(printf("[+] Creating images fader (%i images)\n", images->size));
    ctx->imgf = ImageFader_new(images->size);

    VERBOSE(printf("[+] Creating images timer (%d..%d)\n", delays[BD_IMAGES][0], delays[BD_IMAGES][1]));
    ctx->a_images = Alarm_new(delays[BD_IMAGES][0], delays[BD_IMAGES][1]);
  }

  /* Create colormaps fader and timer */
  if (NULL != colormaps) {
    VERBOSE(printf("[+] Creating colormaps fader (%i colormaps)\n", colormaps->size));
    ctx->cf = CmapFader_new(colormaps->size);

    VERBOSE(printf("[+] Creating colormaps timer (%d..%d)\n", delays[BD_COLORMAPS][0], delays[BD_COLORMAPS][1]));
    ctx->a_cmaps = Alarm_new(delays[BD_COLORMAPS][0], delays[BD_COLORMAPS][1]);
  }

  /* Create sequences timer */
  VERBOSE(printf("[+] Creating sequences timer (%d..%d)\n", delays[BD_SEQUENCES][0], delays[BD_SEQUENCES][1]));
  ctx->a_random = Alarm_new(delays[BD_SEQUENCES][0], delays[BD_SEQUENCES][1]);
  ctx->random_mode = BR_NONE;

#ifdef WITH_WEBCAM
  /* Create webcams timer */
  if (ctx->webcams > 1) {
    VERBOSE(printf("[+] Creating webcams timer (%d..%d)\n", delays[BD_WEBCAMS][0], delays[BD_WEBCAMS][1]));
    ctx->a_webcams = Alarm_new(delays[BD_WEBCAMS][0], delays[BD_WEBCAMS][1]);
    Alarm_init(ctx->a_webcams);
  }
#endif
}


void
biniou_get_delay(const enum RandomDelays what, int *min, int *max)
{
  *min = delays[what][0];
  *max = delays[what][1];
}


void
biniou_set_delay(const enum RandomDelays what, const int min, const int max)
{
  if ((min < 1) || (max < 1)) {
    xerror("Set random delay for %s: min (%d) and max (%d) must be >= 1\n",
           delay_names[what], min, max);
  }

  if (max < min) {
    xerror("Set random delay for %s: max (%d) must be >= min (%d)\n",
           delay_names[what], max, min);
  }

#ifdef DEBUG
  printf("[i] Setting min..max delays for %s: %d..%d\n", delay_names[what], min, max);
#endif
  delays[what][0] = min;
  delays[what][1] = max;
}
