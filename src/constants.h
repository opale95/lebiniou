/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_CONSTANTS_H
#define __BINIOU_CONSTANTS_H

// maximum logical buffer size
#ifndef FIXED
extern unsigned short WIDTH, HEIGHT;
#endif
#define BUFFSIZE      (unsigned long)(WIDTH*HEIGHT)
#define RGB_BUFFSIZE  (3*BUFFSIZE)
#define RGBA_BUFFSIZE (4*BUFFSIZE)

// min/max indices
#define MINX       (0)
#define MINY       (0)
#define MAXX       (WIDTH-1)
#define MAXY       (HEIGHT-1)

// misc
#define HWIDTH     (WIDTH>>1)
#define HHEIGHT    (HEIGHT>>1)

#define HMAXX      (MAXX>>1)
#define HMAXY      (MAXY>>1)

#define CENTERX    (HWIDTH-1)
#define CENTERY    (HHEIGHT-1)

#define MINSCREEN  ((WIDTH < HEIGHT) ? WIDTH : HEIGHT)
#define MAXSCREEN  ((WIDTH >= HEIGHT) ? WIDTH : HEIGHT)

#define HMINSCREEN (MINSCREEN>>1)
#define HMAXSCREEN (MAXSCREEN>>1)

// character buffers
#define MAXLEN     _POSIX2_LINE_MAX

// maximum length of a sequence
#define MAX_SEQ_LEN  254

// handy shortcuts
#define DEC(val, maxval) do { if (!val) val = maxval-1; else --val; } while (0)
#define INC(val, maxval) do { if (++val == maxval) val = 0; } while (0)

// settings
#define JSON_SETTINGS PACKAGE_NAME ".json"

// 3D z-buffer
#define ZMAX 6.0

// volume scaling
#define VOLUME_SCALE_STEP 0.1

// delay for ancillary threads (OSD, webcams)
#define THREADS_DELAY 100

// default fps
#define DEFAULT_MAX_FPS 25

// jackaudio
#define JACKAUDIO_DEFAULT_LEFT  "system:capture_1"
#define JACKAUDIO_DEFAULT_RIGHT "system:capture_2"

// sequences directory
#define SEQUENCES_DIR "." PACKAGE_NAME "/sequences"

#endif // __BINIOU_CONSTANTS_H
