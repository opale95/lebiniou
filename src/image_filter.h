/*
 *  Copyright 1994-2021 Olivier Girondel
 *  Copyright 2019-2021 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_IMAGE_FILTER_H
#define __BINIOU_IMAGE_FILTER_H

#include "buffer_8bits.h"

enum FilterType { FT_GENERIC = 0,
                  FT_BLUR1_3x3,
                  FT_BLUR2_3x3,
                  FT_BLUR4_3x3,
                  FT_VBLUR_3x3,
                  FT_HBLUR_3x3,
                  FT_DBLUR1_3x3,
                  FT_DBLUR2_3x3,
                  FT_BLUR_GAUSSIAN_3x3
                } FilterType_e;

enum BorderMode { BM_NONE = 0, BM_CLEAR, BM_LOCAL, BM_TOROIDAL, BM_NB } BorderMode_e;
const char *border_list[BM_NB] = { "None", "Clear", "Local", "Toroidal" };

/* filter size must be size*size */
void image_filter_average(Buffer8_t *dst, const Buffer8_t *src,
                          enum FilterType type, enum BorderMode borders, uint16_t size, uint16_t *filter);

#endif /* __BINIOU_IMAGE_FILTER_H */
