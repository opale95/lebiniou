/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"
#include "biniou.h"
#include "settings.h"


#ifdef WITH_WEBCAM
extern uint8_t hflip, vflip;
#endif
extern enum startModes start_mode;
extern const char *start_modes[SM_NB];
extern uint8_t usage_statistics;


json_t *
Context_settings(const Context_t *ctx)
{
  Settings_lock();
  json_t *current_settings = Settings_get();
  // DEBUG_JSON("current_settings", current_settings);
  json_t *input = json_object_get(current_settings, "input");

  if (NULL == input) {
    input = json_object();
    if (NULL != ctx->input_plugin) {
      json_object_set_new(input, "name", json_string(ctx->input_plugin->name));
      if (is_equal(ctx->input_plugin->name, "jackaudio")) {
        json_object_set_new(input, "jackaudioLeft", json_string(JACKAUDIO_DEFAULT_LEFT));
        json_object_set_new(input, "jackaudioRight", json_string(JACKAUDIO_DEFAULT_RIGHT));
      }
    } else {
      json_object_set_new(input, "name", json_null());
    }
  }
  json_object_set_new(input, "allInputPlugins", json_strtok(INPUT_PLUGINS, ","));
  json_object_set_new(input, "volumeScale", json_real(Context_get_volume_scale(ctx)));

  json_t *screen = json_object();
  json_object_set_new(screen, "width", json_integer(WIDTH));
  json_object_set_new(screen, "height", json_integer(HEIGHT));
  json_object_set_new(screen, "fixed",
#ifdef FIXED
                      json_true()
#else
                      json_false()
#endif
                      );

  json_t *engine = json_object();
  json_object_set_new(engine, "startMode", json_string(start_modes[start_mode]));
  json_object_set_new(engine, "randomMode", json_integer(ctx->random_mode));
  json_object_set_new(engine, "maxFps", json_integer(ctx->max_fps));
  json_object_set_new(engine, "fadeDelay", json_integer(fade_delay));
  int colormaps_min, colormaps_max;
  int images_min, images_max;
  int sequences_min, sequences_max;
  biniou_get_delay(BD_COLORMAPS, &colormaps_min, &colormaps_max);
  biniou_get_delay(BD_IMAGES, &images_min, &images_max);
  biniou_get_delay(BD_SEQUENCES, &sequences_min, &sequences_max);
#ifdef WITH_WEBCAM
  int webcams_min, webcams_max;
  biniou_get_delay(BD_WEBCAMS, &webcams_min, &webcams_max);
#endif
  json_object_set_new(engine, "colormapsMin", json_integer(colormaps_min));
  json_object_set_new(engine, "colormapsMax", json_integer(colormaps_max));
  json_object_set_new(engine, "autoColormapsMode", json_string(Shuffler_mode2str(ctx->cf->shf->mode)));
  json_object_set_new(engine, "imagesMin", json_integer(images_min));
  json_object_set_new(engine, "imagesMax", json_integer(images_max));
  if (NULL != ctx->imgf) {
    json_object_set_new(engine, "autoImagesMode", json_string(Shuffler_mode2str(ctx->imgf->shf->mode)));
  }
  json_object_set_new(engine, "sequencesMin", json_integer(sequences_min));
  json_object_set_new(engine, "sequencesMax", json_integer(sequences_max));
  json_object_set_new(engine, "autoSequencesMode", json_string(Shuffler_mode2str(sequences->shuffler->mode)));
#ifdef WITH_WEBCAM
  json_object_set_new(engine, "webcamsMin", json_integer(webcams_min));
  json_object_set_new(engine, "webcamsMax", json_integer(webcams_max));
  if (NULL != ctx->webcams_shuffler) {
    json_object_set_new(engine, "autoWebcamsMode", json_string(Shuffler_mode2str(ctx->webcams_shuffler->mode)));
  } else {
    json_object_set_new(engine, "autoWebcamsMode", json_string(Shuffler_mode2str(BS_CYCLE)));
  }
  json_object_set_new(engine, "webcams", json_integer(ctx->webcams));
  json_object_set_new(engine, "maxCams", json_integer(MAX_CAMS));
  json_object_set_new(engine, "hFlip", json_boolean(hflip));
  json_object_set_new(engine, "vFlip", json_boolean(vflip));
#endif
  json_object_set_new(engine, "flatpak",
#ifdef FLATPAK
                      json_true()
#else
                      json_false()
#endif
                      );

  json_t *misc = json_object();
  json_object_set_new(misc, "homeDir", json_string(g_get_home_dir()));
  json_object_set_new(misc, "desktopSymlink", json_boolean(has_desktop_symlink()));
  json_object_set_new(misc, "statistics", json_boolean(usage_statistics));

  json_t *settings = json_object();
  json_object_set_new(settings, "version", json_integer(SETTINGS_VERSION));
  json_object_set_new(settings, "input", input);
  json_object_set_new(settings, "file", json_string(Settings_get_configuration_file()));
  json_object_set_new(settings, "screen", screen);
  json_object_set_new(settings, "engine", engine);
  json_object_set_new(settings, "themes", Settings_get_all_themes());
  json_object_set_new(settings, "plugins", Settings_get_plugins());
  json_object_set_new(settings, "misc", misc);
  json_object_set_new(settings, "currentSettings", current_settings);
  Settings_unlock();

  return settings;
}
