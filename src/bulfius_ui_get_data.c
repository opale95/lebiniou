/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "bulfius.h"
#include "colormaps.h"
#include "images.h"


int
callback_ui_get_data(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;
  assert(NULL != ctx);
  assert(NULL != colormaps);
  uint8_t randomModes = 0;

  switch (ctx->random_mode) {
  case BR_SCHEMES:
  case BR_SEQUENCES:
    randomModes = 1;
    break;

  case BR_BOTH:
    randomModes = 2;
    break;

  default:
    break;
  }

  json_t *res = json_pack("{sisisisbsb}",
                          "colormaps", colormaps->size,
                          "images", (NULL != images) ? images->size : 0,
                          "randomModes", randomModes,
                          "autoColormaps", ctx->auto_colormaps,
                          "autoImages", ctx->auto_images);
  ulfius_set_json_body_response(response, 200, res);
  json_decref(res);
  ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");

  return U_CALLBACK_COMPLETE;
}
