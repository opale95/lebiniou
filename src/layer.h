/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_LAYER_H
#define __BINIOU_LAYER_H

#include "plugin.h"


enum LayerMode { LM_NONE = 0, LM_NORMAL, LM_OVERLAY, LM_AND, LM_OR, LM_XOR, LM_AVERAGE, LM_INTERLEAVE, LM_RANDOM, LM_BANDPASS, NB_LAYER_MODES };


typedef struct Layer_s {
  Plugin_t      *plugin;
  json_t        *plugin_parameters;
  enum LayerMode mode;
} Layer_t;


Layer_t *Layer_new(Plugin_t *);
void Layer_delete(Layer_t *);

Layer_t *Layer_copy(const Layer_t *);

enum LayerMode LayerMode_from_string(const char *);
const char *LayerMode_to_string(const enum LayerMode);
const char *LayerMode_to_OSD_string(const enum LayerMode);

json_t *layer_modes(); // return all layer modes

#endif /* __BINIOU_LAYER_H */
