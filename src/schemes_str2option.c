/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "schemes.h"


enum PluginOptions Schemes_str2option(const char *str) {
  if (is_equal(str, "BO_SFX")) {
    return BO_SFX;
  }
  if (is_equal(str, "BO_GFX")) {
    return BO_GFX;
  }
  if (is_equal(str, "BO_BLUR")) {
    return BO_BLUR;
  }
  if (is_equal(str, "BO_DISPLACE")) {
    return BO_DISPLACE;
  }
  if (is_equal(str, "BO_LENS")) {
    return BO_LENS;
  }
  if (is_equal(str, "BO_SCROLL")) {
    return BO_SCROLL;
  }
  if (is_equal(str, "BO_MIRROR")) {
    return BO_MIRROR;
  }
  if (is_equal(str, "BO_ROLL")) {
    return BO_ROLL;
  }
  if (is_equal(str, "BO_WARP")) {
    return BO_WARP;
  }
  if (is_equal(str, "BO_HOR")) {
    return BO_HOR;
  }
  if (is_equal(str, "BO_VER")) {
    return BO_VER;
  }
  if (is_equal(str, "BO_COLORMAP")) {
    return BO_COLORMAP;
  }
  if (is_equal(str, "BO_SPLASH")) {
    return BO_SPLASH;
  }
  if (is_equal(str, "BO_IMAGE")) {
    return BO_IMAGE;
  }
  if (is_equal(str, "BO_NORANDOM")) {
    return BO_NORANDOM;
  }
  if (is_equal(str, "BO_WEBCAM")) {
    return BO_WEBCAM;
  }
  if (is_equal(str, "BO_UNIQUE")) {
    return BO_UNIQUE;
  }
  if (is_equal(str, "BO_FIRST")) {
    return BO_FIRST;
  }
  if (is_equal(str, "BO_LAST")) {
    return BO_LAST;
  }
  if (is_equal(str, "BO_SCHEMES")) {
    return BO_SCHEMES;
  }
  fprintf(stderr, "%s: %s is not a valid option\n", __func__, str);

  return BO_NONE;
};
