/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
// #include "defaults.h"


extern uint16_t demo_port;
uint8_t demo_started = 0;


void
Context_start_demo(Context_t *ctx)
{
  // Initialize instance
  if (ulfius_init_instance(&ctx->demo_instance, demo_port, NULL, NULL) != U_OK) {
    xerror("ulfius_init_instance error on port %d, aborting\n", demo_port);
  }

  // Endpoint list declaration
  ulfius_add_endpoint_by_val(&ctx->demo_instance, BULFIUS_GET, BULFIUS_DEMO_WS, NULL, 0, &callback_preview_websocket, ctx);

  // Start the framework
  const char *key_pem = getenv("LEBINIOU_DEMO_KEY_PEM");
  const char *cert_pem = getenv("LEBINIOU_DEMO_CERT_PEM");
  if ((NULL != key_pem) && (NULL != cert_pem)) {
    if (ulfius_start_secure_framework(&ctx->demo_instance, key_pem, cert_pem) == U_OK) {
      VERBOSE(printf("[i] Started secure ulfius framework on port %d\n", ctx->demo_instance.port));
    } else {
      xerror("Error starting secure ulfius framework on port %d\n", ctx->demo_instance.port);
    }
  } else {
    if (ulfius_start_framework(&ctx->demo_instance) == U_OK) {
      VERBOSE(printf("[i] Started ulfius framework on port %d\n", ctx->demo_instance.port));
    } else {
      xerror("Error starting ulfius framework on port %d\n", ctx->demo_instance.port);
    }
  }

  demo_started = 1;
}


void
Context_stop_demo(Context_t *ctx)
{
  ulfius_remove_endpoint_by_val(&ctx->demo_instance, BULFIUS_GET, BULFIUS_DEMO_WS, NULL);

  ulfius_stop_framework(&ctx->demo_instance);
  ulfius_clean_instance(&ctx->demo_instance);
}
