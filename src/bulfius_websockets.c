/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "plugins.h"
#include "vui.h"

// #define DEBUG_WS

static void
websocket_onclose_callback(const struct _u_request *request,
                           struct _websocket_manager *websocket_manager,
                           void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;

#ifdef DEBUG_WS
  VERBOSE(printf("[i] %s (%s): end session for client: %p\n", __FILE__, __func__, websocket_manager));
#endif
  xpthread_mutex_lock(&ctx->ws_clients_mutex);
  ctx->ws_clients = g_slist_remove(ctx->ws_clients, (gpointer)websocket_manager);
  xpthread_mutex_unlock(&ctx->ws_clients_mutex);
}


static void
websocket_manager_callback(const struct _u_request *request,
                           struct _websocket_manager *websocket_manager,
                           void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;
  int ret;

#if ULFIUS_VERSION_NUMBER >= 0x020703
  websocket_manager->keep_messages = U_WEBSOCKET_KEEP_NONE;
#endif
  xpthread_mutex_lock(&ctx->ws_clients_mutex);
  ctx->ws_clients = g_slist_prepend(ctx->ws_clients, (gpointer)websocket_manager);
  xpthread_mutex_unlock(&ctx->ws_clients_mutex);
#ifdef DEBUG_WS
  VERBOSE(printf("[i] %s (%s): new session, client: %p\n", __FILE__, __func__, websocket_manager));
#endif
  while (ctx->running && (ret = (ulfius_websocket_wait_close(websocket_manager, 2000) == U_WEBSOCKET_STATUS_OPEN))) {
#ifdef DEBUG_WS
    // VERBOSE(printf("[i] %s (%s): websocket_manager_callback: websocket still open: %s\n",
    // __FILE__, __func__, ret ? "true" : "false"));
#endif
  }
}


static void
process_legacy_command(Context_t *ctx, const char *cmd, const struct _websocket_manager *websocket_manager)
{
#ifdef DEBUG_WS
  xdebug("[i] %s (%s) %d: JSON command '%s'\n", __FILE__, __func__, __LINE__, cmd);
#endif
  const int command = str2command(cmd);

  if ((command != -1) && is_allowed(command)) {
    json_t *res = Context_process_command(ctx, (const enum Command)command, websocket_manager, BC_WEB);

    if (NULL != res) {
#ifdef DEBUG_WS
      char *str = json_dumps(res, 0);
      xdebug("[i] %s (%s) %d: JSON command '%s', res: '%s'\n", __FILE__, __func__, __LINE__, cmd, str);
      xfree(str);
#endif
      json_decref(res);
    }
  }
}


static void
process_ui_command(Context_t *ctx, const char *cmd, const json_t *arg, const uint8_t no_reply, struct _websocket_manager *websocket_manager)
{
#ifdef DEBUG_WS
  xdebug("[i] %s (%s) %d: JSON uiCommand --- noReply: %d\n", __FILE__, __func__, __LINE__, no_reply);
  if (json_is_string(arg)) {
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, arg: %s\n", __FILE__, __func__, __LINE__, cmd, json_string_value(arg));
  } else if (json_is_integer(arg)) {
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, arg: %lld\n", __FILE__, __func__, __LINE__, cmd, json_integer_value(arg));
  } else if (json_is_real(arg)) {
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, arg: %f\n", __FILE__, __func__, __LINE__, cmd, json_real_value(arg));
  } else if (json_is_null(arg)) {
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, no argument\n", __FILE__, __func__, __LINE__, cmd);
  } else if (json_is_boolean(arg)) {
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, arg: %s\n", __FILE__, __func__, __LINE__, cmd, json_boolean_value(arg) ? "true" : "false");
  } else if (json_is_array(arg)) {
    char *str = json_dumps(arg, 0);
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, arg: %s\n", __FILE__, __func__, __LINE__, cmd, str);
    xfree(str);
  } else if (json_is_object(arg)) {
    char *str = json_dumps(arg, 0);
    xdebug("[i] %s (%s) %d: JSON uiCommand %s, arg: '%s'\n", __FILE__, __func__, __LINE__, cmd, str);
    xfree(str);
  } else {
    xerror("Unexpected argument type\n");
  }
#endif
  const int command = str2command(cmd);

  if ((command != -1) && is_allowed(command)) {
    json_t *res = Context_process_ui_command(ctx, (const enum Command)command, arg, BC_WEB);

    if (NULL == res) {
      json_t *json = json_pack("{ssss}", "uiCommand", command2str(command), "result", "ok");
#ifdef DEBUG_WS
      char *str1 = json_dumps(json, 0);
      char *str2 = json_dumps(arg, 0);
      xdebug("[i] %s (%s) %d: JSON uiCommand result '%s', arg: '%s': %s\n", __FILE__, __func__, __LINE__, cmd, str1, str2);
      xfree(str2);
      xfree(str1);
#endif
      bulfius_websocket_broadcast_json_message(ctx, json, websocket_manager, no_reply);
      json_decref(json);
    } else {
      json_t *json = json_pack("{ssso}", "uiCommand", command2str(command), "result", res);
#ifdef DEBUG_WS
      char *str1 = json_dumps(arg, 0);
      char *str2 = json_dumps(res, 0);
      xdebug("[i] %s (%s) %d: JSON uiCommand result '%s', arg: '%s': %s\n", __FILE__, __func__, __LINE__, cmd, str1, str2);
      xfree(str2);
      xfree(str1);
#endif
      if (command == UI_CMD_CONNECT) {
#if ULFIUS_VERSION_NUMBER < 0x020609
        char *str = json_dumps(json, 0);
        ulfius_websocket_send_message(websocket_manager, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#else // ULFIUS_VERSION_NUMBER >= 0x020609
        ulfius_websocket_send_json_message(websocket_manager, json);
#endif
#if ULFIUS_VERSION_NUMBER < 0x020609
        xfree(str);
#endif
#if ULFIUS_VERSION_NUMBER < 0x020703
        struct _websocket_message *msg = ulfius_websocket_pop_first_message(websocket_manager->message_list_outcoming);
        if (NULL != msg) {
          ulfius_clear_websocket_message(msg);
        }
#endif
      } else {
        bulfius_websocket_broadcast_json_message(ctx, json, websocket_manager, no_reply);
      }
      json_decref(json);
    }
  }
}


static void
process_vui_command(Context_t *ctx, const char *cmd, const json_t *arg, struct _websocket_manager *websocket_manager)
{
  const int command = str2command(cmd);

  if ((command != -1) && is_allowed(command)) {
    // DEBUG_JSON("arg", arg, 1);
    json_t *res = Context_process_vui_command(ctx, (const enum Command)command, arg, BC_WEB);

    if (NULL == res) {
      json_t *json = json_pack("{ssss}", "vuiCommand", command2str(command), "result", "ok");
#ifdef DEBUG_WS
      char *str1 = json_dumps(json, 0);
      char *str2 = json_dumps(arg, 0);
      xdebug("[i] %s (%s) %d: JSON vuiCommand result '%s', arg: '%s': %s\n", __FILE__, __func__, __LINE__, cmd, str1, str2);
      xfree(str2);
      xfree(str1);
#endif
      bulfius_websocket_broadcast_json_message_all(ctx, json);
      json_decref(json);
    } else {
      json_t *json = json_pack("{ssso}", "vuiCommand", command2str(command), "result", res);
#ifdef DEBUG_WS
      char *str1 = json_dumps(arg, 0);
      char *str2 = json_dumps(res, 0);
      xdebug("[i] %s (%s) %d: JSON vuiCommand result '%s', arg: '%s': %s\n", __FILE__, __func__, __LINE__, cmd, str1, str2);
      xfree(str2);
      xfree(str1);
#endif
      if (command == VUI_CONNECT) {
#if ULFIUS_VERSION_NUMBER < 0x020609
        char *str = json_dumps(json, 0);
        ulfius_websocket_send_message(websocket_manager, U_WEBSOCKET_OPCODE_TEXT, strlen(str), str);
#else // ULFIUS_VERSION_NUMBER >= 0x020609
        ulfius_websocket_send_json_message(websocket_manager, json);
#endif
#if ULFIUS_VERSION_NUMBER < 0x020609
        xfree(str);
#endif
#if ULFIUS_VERSION_NUMBER < 0x020703
        struct _websocket_message *msg = ulfius_websocket_pop_first_message(websocket_manager->message_list_outcoming);
        if (NULL != msg) {
          ulfius_clear_websocket_message(msg);
        }
#endif
      } else {
        bulfius_websocket_broadcast_json_message_all(ctx, json);
      }
      json_decref(json);
    }
  }
}


static void
process_json_payload(Context_t *ctx, const json_t *payload, struct _websocket_manager *websocket_manager)
{
  assert(NULL != ctx);
  json_t *cmd = json_object_get(payload, "command");

  if (NULL != cmd) {
    if (json_is_string(cmd)) {
      process_legacy_command(ctx, json_string_value(cmd), websocket_manager);
    }
    return;
  }

  cmd = json_object_get(payload, "uiCommand");
  if (NULL != cmd) {
    if (json_is_string(cmd)) {
      process_ui_command(ctx, json_string_value(cmd), json_object_get(payload, "arg"), json_boolean_value(json_object_get(payload, "noReply")), websocket_manager);
    }
    return;
  }

  cmd = json_object_get(payload, "vuiCommand");
  if (NULL != cmd) {
    if (json_is_string(cmd)) {
      process_vui_command(ctx, json_string_value(cmd), json_object_get(payload, "arg"), websocket_manager);
    }
    return;
  }
}


static void
websocket_incoming_message_callback(const struct _u_request *request,
                                    struct _websocket_manager *websocket_manager,
                                    const struct _websocket_message* last_message,
                                    void *user_data)
{
  Context_t *ctx = (Context_t *)user_data;

  if (last_message->opcode == U_WEBSOCKET_OPCODE_TEXT) {
#ifdef DEBUG_WS
    // VERBOSE(printf("[i] %s (%s): text payload '%.*s'\n", __FILE__, __func__, (int)last_message->data_len, last_message->data));
#endif
    json_t *payload = json_loadb(last_message->data, last_message->data_len, 0, NULL);

    if (NULL != payload) {
      SequenceManager_lock(ctx->sm);
      process_json_payload(ctx, payload, websocket_manager);
      SequenceManager_unlock(ctx->sm);
      json_decref(payload);
    }
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_BINARY) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): binary payload\n", __FILE__, __func__));
#endif
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_CLOSE) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): got a CLOSE message\n", __FILE__, __func__));
#endif
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_PING) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): got a PING message\n", __FILE__, __func__));
#endif
  } else if (last_message->opcode == U_WEBSOCKET_OPCODE_PONG) {
#ifdef DEBUG_WS
    VERBOSE(printf("[i] %s (%s): got a PONG message\n", __FILE__, __func__));
#endif
  }
#if ULFIUS_VERSION_NUMBER < 0x020703
  struct _websocket_message *msg = ulfius_websocket_pop_first_message(websocket_manager->message_list_incoming);
  if (NULL != msg) {
    ulfius_clear_websocket_message(msg);
  }
#endif
}


int
callback_websocket(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  int ret;

  if ((ret = ulfius_set_websocket_response(response, NULL, NULL,
             &websocket_manager_callback, user_data,
             &websocket_incoming_message_callback, user_data,
             &websocket_onclose_callback, user_data)) == U_OK) {
#if ULFIUS_VERSION_NUMBER >= 0x020700
    ulfius_add_websocket_deflate_extension(response);
#endif
    return U_CALLBACK_CONTINUE;
  } else {
    return U_CALLBACK_ERROR;
  }
}


void
bulfius_websocket_broadcast_json_message(struct Context_s *ctx, json_t *message, const struct _websocket_manager *from, const uint8_t no_reply)
{
    if (NULL != ctx->ws_clients) {
#if ULFIUS_VERSION_NUMBER < 0x020609
      char *payload = json_dumps(message, 0);
#endif
      xpthread_mutex_lock(&ctx->ws_clients_mutex);
      for (GSList *client = ctx->ws_clients; NULL != client; client = client->next) {
        struct _websocket_manager *websocket_manager = (struct _websocket_manager *)client->data;
        if (no_reply && (websocket_manager == from)) {
          continue;
        }
#if ULFIUS_VERSION_NUMBER >= 0x020609
        ulfius_websocket_send_json_message(websocket_manager, message);
#else
        ulfius_websocket_send_message(websocket_manager, U_WEBSOCKET_OPCODE_TEXT, strlen(payload), payload);
#endif
#if ULFIUS_VERSION_NUMBER < 0x020703
        struct _websocket_message *msg = ulfius_websocket_pop_first_message(websocket_manager->message_list_outcoming);
        if (NULL != msg) {
          ulfius_clear_websocket_message(msg);
        }
#endif
      }
      xpthread_mutex_unlock(&ctx->ws_clients_mutex);
#if ULFIUS_VERSION_NUMBER < 0x020609
      xfree(payload);
#endif
    }
}


void
bulfius_websocket_broadcast_json_message_all(struct Context_s *ctx, json_t *message)
{
  bulfius_websocket_broadcast_json_message(ctx, message, NULL, 0);
}
