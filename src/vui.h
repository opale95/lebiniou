/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __VUI_H
#define __VUI_H

#include "context.h"


json_t *vui_connect(Context_t *);
json_t *vui_delete_sequences(Context_t *, const json_t *);
json_t *vui_rename_sequence(Context_t *, const json_t *);
json_t *vui_selector_change(Context_t *, const json_t *);
json_t *vui_shortcut(Context_t *, const json_t *);

#endif // __VUI_H
