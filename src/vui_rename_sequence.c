/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"
#include "sequences.h"


json_t *
vui_rename_sequence(Context_t *ctx, const json_t *arg)
{
  json_t *res = NULL;

  const json_t *id_j = json_object_get(arg, "id");
  const json_t *old_name_j = json_object_get(arg, "oldName");
  const json_t *new_name_j = json_object_get(arg, "newName");

  if (json_is_integer(id_j) && json_is_string(old_name_j) && json_is_string(new_name_j)) {
    const uint64_t id = json_integer_value(id_j);
    const char *old_name = json_string_value(old_name_j);
    const char *new_name = json_string_value(new_name_j);

    if (safe_filename(old_name) && safe_filename(new_name)) {
#ifdef DEBUG_COMMANDS
      printf(">>> VUI_RENAME_SEQUENCE: id=%"PRIu64" old_name: '%s', new_name: '%s'\n", id, old_name, new_name);
#endif
      GList *item = Sequences_find(id);
      if (NULL != item) {
        Sequence_t *seq = (Sequence_t *)item->data;
        gchar *old_file = g_strdup_printf("%s/%s.json", Sequences_get_dir(), old_name);
        gchar *new_file = g_strdup_printf("%s/%s.json", Sequences_get_dir(), new_name);

        if (rename(old_file, new_file) != 0) {
          res = json_pack("{ss}", "error");
        }
        g_free(old_file);
        g_free(new_file);
        xfree(seq->name);
        seq->name = strdup(new_name);
      } else {
        res = json_pack("{ss}", "error", "inexistent");
      }
    }
  }

  return res;
}
