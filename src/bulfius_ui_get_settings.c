/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "settings.h"


#ifdef WITH_WEBCAM
extern uint8_t hflip, vflip;
#endif
extern enum startModes start_mode;
extern const char *start_modes[SM_NB];
extern uint8_t usage_statistics;


int
callback_ui_get_settings(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const Context_t *ctx = (const Context_t *)user_data;
  assert(NULL != ctx);
  json_t *settings = NULL;

  if (NULL == u_map_get(request->map_url, "vui")) {
    settings = Context_settings(ctx);
  } else {
    settings = json_pack("{si s{ss? so} s{ss sb} so s{ss ss sb sb} so}",

                         "version", SETTINGS_VERSION,

                         "input",
                         "name", (NULL != ctx->input_plugin) ? ctx->input_plugin->name : NULL,
                         "allInputPlugins", json_strtok(INPUT_PLUGINS, ","),

                         "engine",
                         "startMode", start_modes[start_mode],
                         "flatpak",
#ifdef FLATPAK
                         1,
#else
                         0,
#endif

                         "themes", Settings_get_all_themes(),

                         "misc",
                         "homeDir", g_get_home_dir(),
                         "configurationFile", Settings_get_configuration_file(),
                         "hasDesktopSymlink", has_desktop_symlink(),
                         "optIn", usage_statistics,

                         "plugins", Settings_get_plugins()
                         );
#ifndef FIXED
    json_object_set_new(settings, "screen", json_pack("{si si}",
                                                      "width", WIDTH,
                                                      "height", HEIGHT));
#endif
#ifdef WITH_WEBCAM
    json_object_set_new(settings, "webcam", json_pack("{si sb sb}",
                                                      "maxCams", MAX_CAMS,
                                                      "hFlip", hflip,
                                                      "vFlip", vflip));
#endif
  }

  ulfius_set_json_body_response(response, 200, settings);
  json_decref(settings);
  ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");

  return U_CALLBACK_COMPLETE;
}
