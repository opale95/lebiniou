/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "globals.h"


// Returns an array with all known sequences
// FIXME need to lock the sequences ?
int
callback_get_sequences(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  // Context_t *ctx = (Context_t *)user_data;
  // assert(NULL != ctx);

  // SequenceManager_lock(ctx->sm);
  json_t *payload = json_array();
  assert(NULL != sequences);
  for (GList *seq = sequences->seqs; NULL != seq; seq = g_list_next(seq)) {
    Sequence_t *s = (Sequence_t *)seq->data;
    json_array_append_new(payload, json_pack("{sI ss*}", "id", s->id, "name", s->name));
  }
  // SequenceManager_unlock(ctx->sm);
  ulfius_set_json_body_response(response, 200, payload);
  ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");
  json_decref(payload);

  return U_CALLBACK_COMPLETE;
}
