/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "globals.h"
#include "settings.h"


int
callback_get_plugins(const struct _u_request *request, struct _u_response *response, void *user_data)
{
  const char *option = u_map_get(request->map_url, "option");
  enum PluginOptions opt = BO_NONE;

  if (NULL != option) {
    opt = Schemes_str2option(option);
  }

  if ((NULL != plugins) && (NULL != plugins->plugins)) {
    json_t *res = json_array();

    for (int i = 0; i < plugins->size; i++) {
      const Plugin_t *p = plugins->plugins[i];

      if ((NULL == option) || (*p->options & opt)) {
        json_t *plugin = json_object();

        json_object_set_new(plugin, "name", json_string(p->name));
        json_object_set_new(plugin, "displayName", json_string(p->dname));
        json_object_set_new(plugin, "selected", json_boolean(p == plugins->selected));
        json_object_set_new(plugin, "favorite", json_boolean(Settings_is_favorite(p->name)));
        json_array_append_new(res, plugin);
      }
    }
    ulfius_set_json_body_response(response, 200, res);
    ulfius_add_header_to_response(response, "Access-Control-Allow-Origin", "*");
    json_decref(res);

    return U_CALLBACK_COMPLETE;
  }

  return U_CALLBACK_ERROR;
}


