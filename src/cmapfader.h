/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_CMAPFADER_H
#define __BINIOU_CMAPFADER_H

#include <jansson.h>
#include "cmap_8bits.h"
#include "fader.h"
#include "commands.h"
#include "shuffler.h"


typedef struct CmapFader_s {
  uint8_t  on;
  Cmap8_t *cur;
  Cmap8_t *dst;
  Fader_t *fader;
  Shuffler_t *shf;
  uint8_t  refresh; /* refresh 8bits drivers (not RGB) */
} CmapFader_t;


CmapFader_t *CmapFader_new(const uint16_t);
void CmapFader_delete(CmapFader_t *);

void CmapFader_set(CmapFader_t *);

void CmapFader_prev(CmapFader_t *);
void CmapFader_prev_n(CmapFader_t *, const uint16_t);
void CmapFader_next(CmapFader_t *);
void CmapFader_next_n(CmapFader_t *, const uint16_t);
void CmapFader_random(CmapFader_t *);
void CmapFader_use(CmapFader_t *, const uint16_t);

void CmapFader_init(CmapFader_t *);
void CmapFader_run(CmapFader_t *);

json_t *CmapFader_command(CmapFader_t *, const enum Command);
json_t *CmapFader_command_result(const CmapFader_t *);

int CmapFader_ring(const CmapFader_t *);

#endif /* __BINIOU_CMAPFADER_H */
