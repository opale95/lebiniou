/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "vui.h"
#include "biniou.h"
#include "defaults.h"
#include "colormaps.h"


extern uint64_t frames;
extern uint8_t  encoding;
char *randomModes[BR_NB] = { "Off", "Sequences", "Schemes", "Mixed" };


json_t *
vui_connect(Context_t *ctx)
{
  json_t *res = NULL;
  int colormaps_min, colormaps_max;
  int images_min, images_max;
  int sequences_min, sequences_max;
#ifdef WITH_WEBCAM
  int webcams_min, webcams_max;
#endif

  biniou_get_delay(BD_COLORMAPS, &colormaps_min, &colormaps_max);
  biniou_get_delay(BD_IMAGES, &images_min, &images_max);
  biniou_get_delay(BD_SEQUENCES, &sequences_min, &sequences_max);
#ifdef WITH_WEBCAM
  biniou_get_delay(BD_WEBCAMS, &webcams_min, &webcams_max);
#endif

  json_t *modes = json_pack("{s{ss s[ii]} s{ss s[ii]}}",
                            "autoColormaps",
                            "mode", Shuffler_mode2str(ctx->cf->shf->mode),
                            "range", colormaps_min, colormaps_max,
                            "autoSequences",
                            "mode", Shuffler_mode2str(sequences->shuffler->mode),
                            "range", sequences_min, sequences_max
                            );
  if (NULL != ctx->imgf) {
    json_object_set_new(modes, "autoImages", json_pack("{ss s[ii]}",
                                                       "mode", Shuffler_mode2str(ctx->imgf->shf->mode),
                                                       "range", images_min, images_max));
  }
#ifdef WITH_WEBCAM
  json_object_set_new(modes, "autoWebcams", json_pack("{ss s?[ii]}",
                                                      "mode", (ctx->webcams > 1) ? Shuffler_mode2str(ctx->webcams_shuffler->mode) : NULL,
                                                      "range", webcams_min, webcams_max));
#endif

  res = json_pack("{ ss ss? ss so sb sb sb sb ss ss sI si si ss* si si so sf sf so ss* so sb sb so so si so so so si si si si si }",
                  "version", LEBINIOU_VERSION,
                  "ulfius",
#ifdef ULFIUS_VERSION_STR
                  ULFIUS_VERSION_STR
#else
                  NULL
#endif
                  ,
                  "sequenceName", (NULL == ctx->sm->cur->name) ? UNSAVED_SEQUENCE : ctx->sm->cur->name,
                  "sequence", Sequence_to_json(ctx, ctx->sm->cur, 1, 0),
                  "randomSchemes", ((ctx->random_mode == BR_SCHEMES) || (ctx->random_mode == BR_BOTH)),
                  "randomSequences", ((ctx->random_mode == BR_SEQUENCES) || (ctx->random_mode == BR_BOTH)),
                  "autoColormaps", ctx->auto_colormaps,
                  "autoImages", ctx->auto_images,
                  "selectedPlugin", plugins->selected->name,
                  "selectedPluginDname", plugins->selected->dname,
                  "frames", frames,
                  "maxFps", ctx->max_fps,
                  "webcams", ctx->webcams,
                  "lockedPlugin", (NULL != ctx->locked) ? ctx->locked->name : NULL,
                  "bankSet", ctx->bank_set,
                  "bank", ctx->bank,
                  "banks", Context_get_bank_set(ctx, ctx->bank_set),
                  "volumeScale", Context_get_volume_scale(ctx),
                  "fadeDelay", fade_delay,
                  "params3d", Params3d_to_json(&ctx->params3d),
                  "input", (NULL != ctx->input_plugin) ? ctx->input_plugin->name : NULL,
                  "outputPlugins", Context_output_plugins(ctx),
                  "fullScreen", ctx->fullscreen,
                  "encoding", encoding,
                  "allInputPlugins", json_strtok(INPUT_PLUGINS, ","),
                  "allOutputPlugins", json_strtok(OUTPUT_PLUGINS, ","),
                  "rotationFactor", ctx->params3d.rotation_factor,
                  "layerModes", layer_modes(),
                  "shortcuts", Context_get_shortcuts(ctx),
                  "modes", modes,
                  "colormaps", colormaps->size,
                  "images", (NULL != ctx->imgf) ? images->size : 0,
                  "width", WIDTH,
                  "height", HEIGHT,
                  "sequences", sequences->size
                  );

  if (NULL != ctx->input) {
    json_object_set_new(res, "mute", json_boolean(ctx->input->mute));
  }

  return res;
}
