/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_ULFIUS_H
#define __BINIOU_ULFIUS_H

#include <ulfius.h>
#include "context.h"
#include "commands.h"

#define BULFIUS_GET  "GET"
#define BULFIUS_POST "POST"

#define BULFIUS_COLORMAP       "/colormap"
#define BULFIUS_COMMAND        "/command"
#define BULFIUS_COMMAND_FMT    "/:cmd"
#define BULFIUS_COMMANDS       "/commands"
#define BULFIUS_IMAGE          "/image"
#define BULFIUS_FRAME          "/frame"
#define BULFIUS_PARAMETERS     "/parameters"
#define BULFIUS_PARAMETERS_FMT "/:plugin"
#define BULFIUS_PLUGINS        "/plugins"
#define BULFIUS_SEQUENCE       "/sequence"
#define BULFIUS_SEQUENCES      "/sequences"
#define BULFIUS_STATISTICS     "/statistics"
// Web UI
#define BULFIUS_UI_COMMAND         "/ui/command"
#define BULFIUS_UI_ROOT            "/"
#define BULFIUS_UI_TESTING_ROOT    "/testing"
#define BULFIUS_UI_PREVIEW_WS      "/ui/preview"
#define BULFIUS_UI_WEBSOCKET       "/ui"
#define BULFIUS_UI_STATIC          "/ui/static"
#define BULFIUS_UI_STATIC_FMT      "/:file"
#define BULFIUS_UI_FAVICO_URL      "/favicon.ico"
#define BULFIUS_UI_FAVICO_ICO      "www/favicon.ico"
#define BULFIUS_UI_INDEX           "www/index.html"
#define BULFIUS_UI_TESTING_INDEX   "www/testing.html"
#define BULFIUS_UI_SETTINGS_ROOT   "/settings.html"
#define BULFIUS_UI_SETTINGS_INDEX  "www/settings.html"
#define BULFIUS_UI_SETTINGS        "/settings"
#define BULFIUS_UI_DATA            "/data"
#define BULFIUS_UI_PICKER_ROOT     "/picker.html"
#define BULFIUS_UI_PICKER_INDEX    "www/picker.html"
#define BULFIUS_UI_PLUGINS_ROOT    "/plugins.html"
#define BULFIUS_UI_PLUGINS_INDEX   "www/plugins.html"
#define BULFIUS_UI_VIDEO_ROOT      "/videos/code-loop.mp4"
#define BULFIUS_UI_VIDEO_INDEX     "www/videos/code-loop.mp4"
// JavaScript/CSS
#define BULFIUS_UI_JQUERY_MIN_JS_URL     "/jquery.min.js"
#define BULFIUS_UI_JQUERY_MIN_JS         "www/jquery.min.js"
#define BULFIUS_UI_JQUERY_UI_MIN_JS_URL  "/jquery-ui.min.js"
#define BULFIUS_UI_JQUERY_UI_MIN_JS      "www/jquery-ui.min.js"
#define BULFIUS_UI_JQUERY_UI_MIN_CSS_URL "/jquery-ui.min.css"
#define BULFIUS_UI_JQUERY_UI_MIN_CSS     "www/jquery-ui.min.css"
// Vue UI endpoints
#define BULFIUS_VUI_ROOT           BULFIUS_UI_ROOT
#define BULFIUS_VUI_FAVICO_URL     BULFIUS_UI_FAVICO_URL
#define BULFIUS_VUI_CSS_ROOT       "/css"
#define BULFIUS_VUI_IMG_ROOT       "/img"
#define BULFIUS_VUI_JS_ROOT        "/js"
#define BULFIUS_VUI_STATIC_FMT     ":file"
#define BULFIUS_VUI_INDEX_FILE     "index.html"
#define BULFIUS_VUI_FAVICO_ICO     "favicon.ico"
#define BULFIUS_VUI_INDEX  1
#define BULFIUS_VUI_FAVICO 2
#define BULFIUS_VUI_CSS    3
#define BULFIUS_VUI_IMG    4
#define BULFIUS_VUI_JS     5
// Demo endpoint
#define BULFIUS_DEMO_WS BULFIUS_UI_ROOT


// OPTIONS
int callback_options(const struct _u_request *, struct _u_response *, void *);

// GET
int callback_get_colormap(const struct _u_request *, struct _u_response *, void *);
int callback_get_commands(const struct _u_request *, struct _u_response *, void *);
int callback_get_image(const struct _u_request *, struct _u_response *, void *);
int callback_get_frame(const struct _u_request *, struct _u_response *, void *);
int callback_get_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_get_plugins(const struct _u_request *, struct _u_response *, void *);
int callback_get_sequence(const struct _u_request *, struct _u_response *, void *);
int callback_get_sequences(const struct _u_request *, struct _u_response *, void *);
int callback_get_statistics(const struct _u_request *, struct _u_response *, void *);
// GET/Web UI
int callback_ui_get_static(const struct _u_request *, struct _u_response *, void *);
int callback_ui_get_settings(const struct _u_request *, struct _u_response *, void *);
int callback_ui_get_data(const struct _u_request *, struct _u_response *, void *);
// GET/Vue UI
int callback_vui_get_static(const struct _u_request *, struct _u_response *, void *);

// POST
int callback_post_command(const struct _u_request *, struct _u_response *, void *);
int callback_post_parameters(const struct _u_request *, struct _u_response *, void *);
int callback_post_plugins(const struct _u_request *, struct _u_response *, void *);
int callback_post_sequence(const struct _u_request *, struct _u_response *, void *);
int callback_post_sequences(const struct _u_request *, struct _u_response *, void *);
// POST/Web UI
int callback_ui_post_command(const struct _u_request *, struct _u_response *, void *);
int callback_ui_post_settings(const struct _u_request *, struct _u_response *, void *);

// Websockets
int callback_websocket(const struct _u_request *, struct _u_response *, void *);
void bulfius_websocket_broadcast_json_message(struct Context_s *, json_t *n, const struct _websocket_manager *, const uint8_t);
void bulfius_websocket_broadcast_json_message_all(struct Context_s *, json_t *n);
int callback_preview_websocket(const struct _u_request *, struct _u_response *, void *);

// Commands
int is_allowed(const enum Command cmd);
enum Command str2command(const char *);
const char *command2str(const enum Command);

// Reports
void bulfius_post_report(const char *, json_t *, json_t *);

#endif /* __BINIOU_ULFIUS_H */
