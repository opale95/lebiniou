/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "bulfius.h"
#include "context.h"
#include "defaults.h"


extern uint8_t max_fps;
extern uint16_t vui_port;
// static pthread_t ping_thread;
extern uint8_t launch_browser;
uint8_t vui_started = 0;


#if 0
static void *
vui_thread(void *attr)
{
  Context_t *ctx = (Context_t *)attr;

  while (ctx->running) {
    json_t *ping = json_pack("{s{sisisI}}",
                             "ping",
                             "fps", Context_fps(ctx),
                             "maxFps", ctx->max_fps,
                             "uptime", (uint64_t)Timer_elapsed(ctx->timer));
    bulfius_websocket_broadcast_json_message(ctx, ping);
    json_decref(ping);
    ms_sleep(1000);
  }

  return NULL;
}
#endif


void
Context_start_vui(Context_t *ctx)
{
#if 0 // done in the regular vui
  xpthread_mutex_init(&ctx->ws_clients_mutex, NULL);
#endif

  // Initialize instance
  if (ulfius_init_instance(&ctx->vui_instance, vui_port, NULL, NULL) != U_OK) {
    xerror("ulfius_init_instance error on port %d, aborting\n", vui_port);
  }

  // Endpoint list declaration
  // GET
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_ROOT, NULL, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_INDEX);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_FAVICO_URL, NULL, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_FAVICO);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_CSS_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_CSS);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_IMG_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_IMG);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_JS_ROOT, BULFIUS_VUI_STATIC_FMT, 0, &callback_vui_get_static, (void *)BULFIUS_VUI_JS);
  // ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_ROOT, ":file", 0, &callback_vui_get_static, (void *)BULFIUS_VUI_ROOT);
#if 0
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_COLORMAP, NULL, 0, &callback_get_colormap, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_COMMANDS, NULL, 0, &callback_get_commands, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_IMAGE, NULL, 0, &callback_get_image, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_FRAME, NULL, 0, &callback_get_frame, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT, 0, &callback_get_parameters, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_PLUGINS, NULL, 0, &callback_get_plugins, NULL);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_SEQUENCE, NULL, 0, &callback_get_sequence, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_STATISTICS, NULL, 0, &callback_get_statistics, ctx);
  // GET/UI
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_PICKER_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_UI_PICKER_INDEX);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_PLUGINS_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_UI_PLUGINS_INDEX);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_DATA, NULL, 0, &callback_ui_get_data, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_FAVICO_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_FAVICO_ICO);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_TESTING_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_UI_TESTING_INDEX);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_STATIC, BULFIUS_UI_STATIC_FMT, 0, &callback_ui_get_static, NULL);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_SETTINGS_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_UI_SETTINGS_INDEX);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_SETTINGS, NULL, 0, &callback_ui_get_settings, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_VIDEO_ROOT, NULL, 0, &callback_ui_get_static, BULFIUS_UI_VIDEO_INDEX);
  // JavaScript/CSS
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_JQUERY_MIN_JS_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_JQUERY_MIN_JS);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_JQUERY_UI_MIN_JS_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_JQUERY_UI_MIN_JS);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_JQUERY_UI_MIN_CSS_URL, NULL, 0, &callback_ui_get_static, BULFIUS_UI_JQUERY_UI_MIN_CSS);

  // POST
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_COMMAND, BULFIUS_COMMAND_FMT, 0, &callback_post_command, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT, 0, &callback_post_parameters, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_PLUGINS, NULL, 0, &callback_post_plugins, NULL);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_SEQUENCE, NULL, 0, &callback_post_sequence, ctx);
  // POST/UI
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_UI_COMMAND, BULFIUS_COMMAND_FMT, 0, &callback_ui_post_command, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_UI_SETTINGS_ROOT, NULL, 0, &callback_ui_post_settings, ctx);

  // Websocket
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_WEBSOCKET, NULL, 0, &callback_websocket, ctx);
  ulfius_add_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_PREVIEW_WS, NULL, 0, &callback_preview_websocket, ctx);
#endif

  // Start the framework
  if (ulfius_start_framework(&ctx->vui_instance) == U_OK) {
    VERBOSE(printf("[i] Started ulfius framework on port %d\n", ctx->vui_instance.port));
  } else {
    xerror("Error starting ulfius framework on port %d\n", ctx->vui_instance.port);
  }

  // xpthread_create(&ping_thread, NULL, vui_thread, (void *)ctx);
  vui_started = 1;

  if (launch_browser) {
    gchar *cmd = g_strdup_printf("/usr/bin/xdg-open http://localhost:%d", vui_port);
    int ret = system(cmd);
    if (ret == -1) {
      xperror("system");
    }
    g_free(cmd);
  }
}


void
Context_stop_vui(Context_t *ctx)
{
  // xpthread_join(ping_thread, NULL);

  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_FAVICO_URL, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_CSS_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_IMG_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_VUI_JS_ROOT, NULL);
#if 0
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_COLORMAP, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_COMMANDS, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_IMAGE, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_FRAME, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_PLUGINS, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_SEQUENCE, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_STATISTICS, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_TESTING_ROOT, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_STATIC, BULFIUS_UI_STATIC_FMT);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_COMMAND, BULFIUS_COMMAND_FMT);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_PARAMETERS, BULFIUS_PARAMETERS_FMT);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_SEQUENCE, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_POST, BULFIUS_UI_COMMAND, BULFIUS_COMMAND_FMT);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_WEBSOCKET, NULL);
  ulfius_remove_endpoint_by_val(&ctx->vui_instance, BULFIUS_GET, BULFIUS_UI_PREVIEW_WS, NULL);
#endif

  ulfius_stop_framework(&ctx->vui_instance);
  ulfius_clean_instance(&ctx->vui_instance);
#if 0
  xpthread_mutex_destroy(&ctx->ws_clients_mutex);
#endif
}
