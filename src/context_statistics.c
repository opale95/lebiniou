/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include <sys/utsname.h>
#include <ulfius.h>
#include "context.h"
#include "defaults.h"
#include "settings.h"
#include "globals.h"
#include "colormaps.h"
#include "paths.h"
#include "settings.h"


extern uint64_t frames;
static const char *cfg_types[CFG_NB] = { "default", "custom" };
extern uint8_t json_settings_type;


#if GLIB_CHECK_VERSION(2, 64, 0)
static void
add_os_info(json_t *os_info, const char *name, const char *key)
{
  gchar *info = g_get_os_info(key);

  if (NULL != info) {
    json_object_set_new(os_info, name, json_string(info));
    g_free(info);
  }
}
#endif


void
Context_statistics(const Context_t *ctx)
{
  json_t *fields = json_pack("{sIsisisisisisisisisisi}",
                             "elapsed", (json_int_t)Timer_elapsed(ctx->timer),
                             "frames", frames,
                             "sdl2", ctx->commands[BC_SDL2],
                             "rest", ctx->commands[BC_REST],
                             "web", ctx->commands[BC_REST],
                             "plugins", plugins->size,
                             "colormaps", colormaps->size,
                             "images", (NULL == images) ? 0 : images->size,
                             "paths", (NULL == paths) ? 0 : paths->size,
                             "sequences", (NULL == sequences) ? 0 : sequences->size,
                             "schemes", (NULL == schemes) ? 0 : schemes->size);
#ifdef WITH_WEBCAM
  json_object_set_new(fields, "webcams", json_integer(ctx->webcams));
#endif

  // DEBUG_JSON("fields", fields);

  gchar *resolution = g_strdup_printf("%dx%d", WIDTH, HEIGHT);
  json_t *tags = json_pack("{sbsbsssssbsssb}",
                           "debug",
#ifdef DEBUG
                           1,
#else
                           0,
#endif
                           "fixed",
#ifdef FIXED
                           1,
#else
                           0,
#endif
                           "version", LEBINIOU_VERSION,
                           "resolution", resolution,
                           "flatpak",
#ifdef FLATPAK
                           1,
#else
                           0,
#endif
                           "json_settings", cfg_types[json_settings_type],
                           "ci", (NULL != getenv("LEBINIOU_TEST")));
  g_free(resolution);
  if (NULL != ctx->input_plugin) {
    json_object_set_new(tags, "input", json_string(ctx->input_plugin->name));
  }
  for (GSList *outputs = ctx->outputs; NULL != outputs; outputs = g_slist_next(outputs)) {
    Plugin_t *output = (Plugin_t *)outputs->data;

    json_object_set_new(tags, output->name, json_true());
  }

  struct utsname _uname;
  if (!uname(&_uname)) {
    json_object_set_new(tags, "sysname", json_string(_uname.sysname));
    json_object_set_new(tags, "nodename", json_string(_uname.nodename));
    json_object_set_new(tags, "release", json_string(_uname.release));
    json_object_set_new(tags, "wsl",  ((NULL != strstr(_uname.release, "-microsoft")) || (NULL != strstr(_uname.release, "-WSL"))) ? json_true() : json_false());
    json_object_set_new(tags, "kernel", json_string(_uname.version));
    json_object_set_new(tags, "machine", json_string(_uname.machine));
  } else {
    xperror("uname");
  }

  // OS info
#if GLIB_CHECK_VERSION(2, 64, 0)
  add_os_info(tags, "os_name", G_OS_INFO_KEY_NAME);
  add_os_info(tags, "os_pretty_name", G_OS_INFO_KEY_PRETTY_NAME);
  add_os_info(tags, "os_version", G_OS_INFO_KEY_VERSION);
  add_os_info(tags, "os_version_codename", G_OS_INFO_KEY_VERSION_CODENAME);
  add_os_info(tags, "os_version_id", G_OS_INFO_KEY_VERSION_ID);
  add_os_info(tags, "os_id", G_OS_INFO_KEY_ID);
  add_os_info(tags, "os_home_url", G_OS_INFO_KEY_HOME_URL);
#endif

  // Ulfius
#ifdef ULFIUS_VERSION_STR
  json_object_set_new(tags, "ulfius", json_string(ULFIUS_VERSION_STR));
#endif

  // DEBUG_JSON("tags", tags);

  bulfius_post_report("/statistics", fields, tags);
  json_decref(fields);
  json_decref(tags);
}
