/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SETTINGS_H
#define __SETTINGS_H

#include "context.h"


#define SETTINGS_VERSION 2

enum cfg_types { CFG_DEFAULT = 0, CFG_CUSTOM, CFG_NB };

enum startModes { SM_LAST = 0, SM_FIRST, SM_LAST_UPDATED, SM_NB };


void Settings_new();
void Settings_delete();

json_t *Settings_get();
json_t *Settings_get_input();

json_t *Settings_get_plugins();
void Settings_set_plugins(json_t *);
json_t *Settings_get_themes();
json_t *Settings_get_all_themes();

uint8_t Settings_parse_post(Context_t *, json_t *);

const char *Settings_get_configuration_file();
void Settings_set_configuration_file(const char *);

void Settings_load();
void Settings_finish(Context_t *);

void Settings_lock();
void Settings_unlock();

uint8_t Settings_is_favorite(const char *);
uint8_t Settings_switch_favorite(const char *);

#endif // __SETTINGS_H
