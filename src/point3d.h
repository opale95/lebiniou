/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __BINIOU_POINT3D_H
#define __BINIOU_POINT3D_H

typedef struct _Point3d_s {
  float x;
  float y;
  float z;
} _Point3d_t;


typedef union Point3d_u {
  _Point3d_t pos;
  float      coords[3];
} Point3d_t;


static inline Point3d_t
p3d_add(const Point3d_t *p0, const Point3d_t *p1)
{
  Point3d_t p;

  p.pos.x = p0->pos.x + p1->pos.x;
  p.pos.y = p0->pos.y + p1->pos.y;
  p.pos.z = p0->pos.z + p1->pos.z;

  return p;
}


static inline Point3d_t
p3d_sub(const Point3d_t *p0, const Point3d_t *p1)
{
  Point3d_t p;

  p.pos.x = p0->pos.x - p1->pos.x;
  p.pos.y = p0->pos.y - p1->pos.y;
  p.pos.z = p0->pos.z - p1->pos.z;

  return p;
}


static inline Point3d_t
p3d_mul(const Point3d_t *p0, const float f)
{
  Point3d_t p;

  p.pos.x = p0->pos.x * f;
  p.pos.y = p0->pos.y * f;
  p.pos.z = p0->pos.z * f;

  return p;
}


static inline Point3d_t
p3d_div(const Point3d_t *p0, const float f)
{
  Point3d_t p;

  p.pos.x = p0->pos.x / f;
  p.pos.y = p0->pos.y / f;
  p.pos.z = p0->pos.z / f;

  return p;
}

#endif /* __BINIOU_POINT3D_H */
