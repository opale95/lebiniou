/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "globals.h"
#include "defaults.h"
#include "settings.h"

int32_t     WIDTH_ORIGIN, HEIGHT_ORIGIN;
#ifndef FIXED
uint16_t     WIDTH, HEIGHT;
#endif

Plugins_t   *plugins   = NULL;
Sequences_t *sequences = NULL;
Context_t   *context   = NULL;
Schemes_t   *schemes   = NULL;

uint8_t libbiniou_verbose = 1;

uint16_t width = DEFAULT_WIDTH;
uint16_t height = DEFAULT_HEIGHT;

char *input_plugin = NULL;
char *data_dir = NULL;
char *themes = NULL;
enum RandomMode random_mode = BR_BOTH;
#ifdef WITH_WEBCAM
uint8_t desired_webcams = 1;
uint8_t hflip = 0;
uint8_t vflip = 0;
char *video_base;
#else
uint8_t desired_webcams = 0;
#endif

uint8_t max_fps = DEFAULT_MAX_FPS;
uint8_t use_hard_timers = 1;
uint64_t frames = 0;
enum startModes start_mode = SM_LAST;

// sndfile
char *audio_file = NULL;

// start mp4 encoding automatically
uint8_t  encoding = 0;

uint8_t override_webcam = 0;
uint8_t usage_statistics = 1;
uint8_t enable_osd = 0;
uint8_t launch_browser = 1;

// volume scaling */
double volume_scale = 1.0;

