/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "options.h"


PluginType_t pTypes[MAX_OPTIONS] = {
  { BO_NONE, 0 },
  { BO_SFX, 0 },
  { BO_GFX, 0 },
  { BO_BLUR, 0 },
  { BO_DISPLACE, 0 },
  { BO_LENS, 0 },
  { BO_SCROLL, 0 },
  { BO_MIRROR, 0 },
  { BO_ROLL, 0 },
  { BO_WARP, 0 },
  { BO_HOR, 0 },
  { BO_VER, 0 },
  { BO_COLORMAP, 0 },
  { BO_SPLASH, 0 },
  { BO_IMAGE, 0 },
  { BO_NORANDOM, 0 },
  { BO_WEBCAM, 0 },
  { BO_UNIQUE, 0 },
  { BO_FIRST, 0 },
  { BO_LAST, 0 },
  { BO_SCHEMES, 0 }
};
