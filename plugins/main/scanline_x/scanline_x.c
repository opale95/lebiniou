/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "images.h"
enum Direction { LEFTWARDS = 0, RIGHTWARDS, BOUNCE, DIRECTION_NB } Mode_e;
const char *direction_list[DIRECTION_NB] = { "Leftwards", "Rightwards", "Bounce" };
#define SIZE WIDTH
#include "scanline.h"


uint32_t version = 0;
uint32_t options = BO_GFX | BO_IMAGE | BO_SCHEMES;
char dname[] = "Scanline X";
char desc[] = "Horizontal scanline";


void
on_switch_on(Context_t *ctx)
{
  direction = RIGHTWARDS;
  current_direction = 1;
  thickness = 1;
  set_size();
}


void
run(Context_t *ctx)
{
  static int x_line = 0;
  Buffer8_t *dst = NULL;
  Buffer8_t *src = ctx->imgf->cur->buff;

  swap_buffers(ctx);
  dst = passive_buffer(ctx);

  for (uint16_t s = 0; s < size; s++) {
    for (uint16_t j = 0; j <= MAXY; j++) {
      set_pixel_nc(dst, x_line, j, get_pixel_nc(src, x_line, j));
    }

    x_line += current_direction;
    if (x_line > MAXX) {
      if (direction == BOUNCE) {
        x_line = MAXX;
        current_direction = -current_direction;
      } else {
        x_line = 0;
      }
    } else if (x_line < 0) {
      if (direction == BOUNCE) {
        x_line = 0;
        current_direction = -current_direction;
      } else {
        x_line = MAXX;
      }
    }
  }
}
