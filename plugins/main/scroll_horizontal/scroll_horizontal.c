/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"


uint32_t version = 1;
uint32_t options = BO_SCROLL | BO_HOR;
char dname[] = "Scroll horizontal";
char desc[] = "Scroll the screen horizontally";

enum Direction { LEFTWARDS = 0, RIGHTWARDS, RANDOM, DIRECTION_NB } Mode_e;
const char *direction_list[DIRECTION_NB] = { "Leftwards", "Rightwards", "Random" };
#include "scroll.h"


static void
scroll_rl(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  Pixel_t save = src->buffer[0];

  memcpy((void *)dst->buffer, (const void *)(src->buffer+sizeof(Pixel_t)), (BUFFSIZE-1)*sizeof(Pixel_t));
  dst->buffer[BUFFSIZE-1] = save;
}


static void
scroll_lr(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  Pixel_t save = src->buffer[BUFFSIZE-1];

  memcpy((void *)(dst->buffer+sizeof(Pixel_t)), (const void *)src->buffer, (BUFFSIZE-1)*sizeof(Pixel_t));
  dst->buffer[0] = save;
}


static void
set_run_ptr()
{
  switch (direction) {
  case LEFTWARDS:
    run_ptr = &scroll_rl;
    break;

  case RIGHTWARDS:
    run_ptr = &scroll_lr;
    break;

  case RANDOM:
  default:
    run_ptr = b_rand_boolean() ? &scroll_lr : &scroll_rl;
    break;
  }
}
