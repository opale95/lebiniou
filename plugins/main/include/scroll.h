/*
 *  Copyright 1994-2021 Olivier Girondel
 *  Copyright 2019-2021 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __SCROLL_H
#define __SCROLL_H

#include "parameters.h"


static enum Direction direction = RANDOM;
static void (*run_ptr)(struct Context_s *) = NULL;
static void set_run_ptr();


json_t *
get_parameters(const uint8_t fetch_all)
{
  json_t *params = json_object();

  plugin_parameters_add_string_list(params, BPP_DIRECTION, DIRECTION_NB, direction_list, direction, DIRECTION_NB-1, "Direction");

  return params;
}


void
set_parameters(const Context_t *ctx, const json_t *in_parameters)
{
  if (plugin_parameter_parse_string_list_as_int_range(in_parameters, BPP_DIRECTION, DIRECTION_NB, direction_list, (int *)&direction) & PLUGIN_PARAMETER_CHANGED) {
    set_run_ptr();
  }
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters, const uint8_t fetch_all)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters(fetch_all);
}


void
on_switch_on(Context_t *ctx)
{
  set_run_ptr();
}


void
run(Context_t *ctx)
{
  run_ptr(ctx);
}


#endif /* __ROLLER_H */
