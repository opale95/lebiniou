/*
 *  Copyright 1994-2021 Olivier Girondel
 *  Copyright 2019-2021 Laurent Marsac
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef __OSCILLO_H
#define __OSCILLO_H

#include "parameters.h"

static Porteuse_t *P[3] = { NULL, NULL, NULL };
static void set_run_ptr();

/* Parameters */
static int stereo = 0;
static int do_connect = 1;
static double volume_scale = 1;


void set_parameters(const Context_t *ctx, const json_t *in_parameters);

uint8_t
set_parameters_oscillo(const Context_t *ctx, const json_t *in_parameters)
{
  uint8_t changed = 0;
  int channels = stereo ? 2 : 1;

  changed |= plugin_parameter_parse_double_range(in_parameters, BPP_VOLUME_SCALE, &volume_scale) & PLUGIN_PARAMETER_CHANGED;

  // v1 API compat
  changed |= plugin_parameter_parse_int_range(in_parameters, BPP_CHANNELS, &channels) & PLUGIN_PARAMETER_CHANGED;
  plugin_parameter_parse_int_range(in_parameters, BPP_CONNECT, &do_connect);

  // v2 API
  changed |= plugin_parameter_parse_boolean(in_parameters, BPP_STEREO, &stereo) & PLUGIN_PARAMETER_CHANGED;
  plugin_parameter_parse_boolean(in_parameters, BPP_CONNECT, &do_connect);

  return changed;
}


json_t *
get_parameters(const uint8_t fetch_all)
{
  json_t *params = json_object();

  plugin_parameters_add_double(params, BPP_VOLUME_SCALE, volume_scale, 0, 10, 0.1, "Volume scale");
  plugin_parameters_add_boolean(params, BPP_STEREO, stereo, "Separate channels");
  plugin_parameters_add_boolean(params, BPP_CONNECT, do_connect, "Draw with lines");

  return params;
}


json_t *
parameters(const Context_t *ctx, const json_t *in_parameters, const uint8_t fetch_all)
{
  if (NULL != in_parameters) {
    set_parameters(ctx, in_parameters);
  }

  return get_parameters(fetch_all);
}


int8_t
create(Context_t *ctx)
{
  P[0] = Porteuse_new(ctx->input->size, A_MONO);
  P[1] = Porteuse_new(ctx->input->size, A_LEFT);
  P[2] = Porteuse_new(ctx->input->size, A_RIGHT);

  return 1;
}


void
destroy(Context_t *ctx)
{
  for (uint8_t i = 0; i < 3; i++) {
    Porteuse_delete(P[i]);
  }
}


#endif /* __OSCILLO_H */
