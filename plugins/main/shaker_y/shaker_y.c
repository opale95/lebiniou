/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

#include "context.h"
#include "shaker.h"

uint32_t version = 0;
uint32_t options = BO_LENS | BO_SFX;
char dname[] = "Shaker Y";
char desc[] = "Shaker effect";


void
on_switch_on(Context_t *ctx)
{
  /* Initialize parameters */
  volume_scale = 1;
}


void
run(Context_t *ctx)
{
  const Buffer8_t *src = active_buffer(ctx);
  Buffer8_t *dst = passive_buffer(ctx);
  uint16_t j;

  Buffer8_init_mask_3x3(active_buffer(ctx));

  xpthread_mutex_lock(&ctx->input->mutex);
  for (j = 0; j < HEIGHT; j++) {
    uint16_t idx = 0;
    uint16_t i;

    for (i = 0; i < WIDTH; i++) {
      char dx = Input_clamp(ctx->input->data[A_LEFT][idx])  * volume_scale * SOUND_DISTURB_K;
      char dy = Input_clamp(ctx->input->data[A_RIGHT][idx]) * volume_scale * SOUND_DISTURB_K;

      if (++idx == ctx->input->size) {
        idx = 0;
      }

      displace(src, dst, i, j, i + dx, j + dy);
    }
  }
  xpthread_mutex_unlock(&ctx->input->mutex);
}
