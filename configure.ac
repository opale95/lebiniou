AC_INIT([lebiniou],[3.56.1],[olivier@biniou.info])
AC_PREREQ([2.68])
AC_CONFIG_SRCDIR([src/main.c])
AC_CANONICAL_TARGET
AM_INIT_AUTOMAKE([1.11 foreign -Wall -Werror])
m4_ifdef([AM_SILENT_RULES], [AM_SILENT_RULES([yes])])

# ------------------------------------------------------------------
# Versioning
# ------------------------------------------------------------------
BINIOU_MAJOR=3
BINIOU_MINOR=56
BINIOU_PATCH=1
#BINIOU_EXTRA="-RC5"
BINIOU_VERSION="$BINIOU_MAJOR.$BINIOU_MINOR.$BINIOU_PATCH$BINIOU_EXTRA"
AC_SUBST(BINIOU_VERSION)

# Prelude
LDFLAGS="-rdynamic -Wl,-z,now $LDFLAGS"

# Checks for programs
OLD_FLAGS="$CFLAGS"
AC_PROG_CC
CFLAGS="$OLD_FLAGS"
AM_PROG_CC_C_O
AC_PROG_SED

# LFS
AC_SYS_LARGEFILE

m4_undefine([AC_PROG_F77])
m4_defun([AC_PROG_F77],[])

# Check for pkg-config
PKG_PROG_PKG_CONFIG

# Check for uglifyjs
AC_ARG_WITH([uglifyjs],
    AS_HELP_STRING([--without-uglifyjs], [Do not minify .js files]))

if [ test "x${with_uglifyjs}" = "x" ]; then
   AC_CHECK_PROG([UGLIFYJS], [uglifyjs], [yes])
   if [ test "x$UGLIFYJS" = "xyes" ]; then
      UGLIFYJS="uglifyjs"
      with_uglifyjs="yes"
   else
      AC_MSG_ERROR([You must have uglifyjs installed])
   fi
fi
AM_CONDITIONAL([WITH_UGLIFYJS], [test "x${with_uglifyjs}" != "xno"])

# Check for cleancss
AC_ARG_WITH([cleancss],
    AS_HELP_STRING([--without-cleancss], [Do not minify .css files]))

if [ test "x${with_cleancss}" = "x" ]; then
   AC_CHECK_PROG([CLEANCSS], [cleancss], [yes])
   if [ test "x$CLEANCSS" = "xyes" ]; then
      CLEANCSS="cleancss"
      with_cleancss="yes"
   else
      AC_MSG_ERROR([You must have cleancss installed])
   fi
fi
AM_CONDITIONAL([WITH_CLEANCSS], [test "x${with_cleancss}" != "xno"])

# Check for htmlmin
AC_ARG_WITH([htmlmin],
    AS_HELP_STRING([--without-htmlmin], [Do not minify .css files]))

if [ test "x${with_htmlmin}" = "x" ]; then
   AC_CHECK_PROG([HTMLMIN], [htmlmin], [yes])
   if [ test "x$HTMLMIN" = "xyes" ]; then
      HTMLMIN="htmlmin"
      with_htmlmin="yes"
   else
      AC_MSG_ERROR([You must have htmlmin installed])
   fi
fi
AM_CONDITIONAL([WITH_HTMLMIN], [test "x${with_htmlmin}" != "xno"])

# Check OS family/flavor
os_family="unknown"
os_flavor="unknown"
DEFAULT_INPUT_PLUGIN=oss
case "$target_os" in
*kfreebsd*)
os_family=bsd
os_flavor=kfreebsd
;;

dragonfly*|darwin*)
os_family=bsd
LIBS="-ldl $LIBS"
;;

netbsd*)
os_family=bsd
os_flavor=netbsd
export PKG_CONFIG_PATH="$PKG_CONFIG_PATH:/usr/pkg/lib/ffmpeg4/pkgconfig"
;;

freebsd*)
os_family=bsd
os_flavor=freebsd
;;

openbsd*)
os_family=bsd
os_flavor=openbsd
;;

# Hurd
*pc-gnu*)
os_family=linux
os_flavor=hurd
;;

*)
os_family=linux
os_flavor=linux
LIBS="-ldl $LIBS"
DEFAULT_INPUT_PLUGIN=alsa
esac
AC_MSG_CHECKING([for operating system family])
AC_MSG_RESULT([${os_family}])
AC_MSG_CHECKING([for operating system flavor])
AC_MSG_RESULT([${os_flavor}])
AM_CONDITIONAL([OS_LINUX_FAMILY], [test "x${os_family}" = "xlinux"])
AM_CONDITIONAL([OS_NETBSD_OR_OPENBSD], [test "x${os_flavor}" = "xnetbsd" || test "x${os_flavor}" = "xopenbsd"])

# Checks for libraries

# glib-2.0
PKG_CHECK_MODULES([GLIB], [glib-2.0], , [AC_MSG_ERROR([You must have libglib2.0-dev installed])])
CFLAGS="$GLIB_CFLAGS $CFLAGS"

# fftw3
PKG_CHECK_MODULES([FFTW3], [fftw3], , [AC_MSG_ERROR([You must have libfftw3-dev installed])])

# jansson
PKG_CHECK_MODULES([JANSSON], [jansson], , [AC_MSG_ERROR([You must have libjansson-dev installed])])

# freetype-2
PKG_CHECK_MODULES([FT2], [freetype2], , [AC_MSG_ERROR([You must have libfreetype6-dev installed])])

# MagickWand
PKG_CHECK_MODULES([MAGICKWAND], [MagickWand], , [AC_MSG_ERROR([You must have libmagickwand-dev installed])])

# orcania
PKG_CHECK_MODULES([ORCANIA], [liborcania], , [AC_MSG_ERROR([You must have liborcania-dev installed])])

# yder
PKG_CHECK_MODULES([YDER], [libyder], , [AC_MSG_ERROR([You must have libyder-dev installed])])

# ulfius
PKG_CHECK_MODULES([ULFIUS], [libulfius], , [AC_MSG_ERROR([You must have libulfius-dev installed ])])

LIBS="${GLIB_LIBS} ${JANSSON_LIBS} -lm $LIBS"

# We need to know which prefix we used to find some default values
if test "x${prefix}" = "xNONE"; then
prefix="/usr/local"
fi
LEBINIOU_LIBDIR="$prefix/lib"
if test x"$DEB_HOST_MULTIARCH" != "x"; then
LEBINIOU_LIBDIR="$LEBINIOU_LIBDIR/$DEB_HOST_MULTIARCH"
fi
LEBINIOU_SHAREDIR="$prefix/share/lebiniou"
LEBINIOU_DATADIR="${LEBINIOU_SHAREDIR}"
LEBINIOU_PLUGINSDIR="${LEBINIOU_LIBDIR}/lebiniou/plugins"
LEBINIOU_SAMPLESDIR="${LEBINIOU_SHAREDIR}/sequences/json"
LEBINIOU_WWWDIR="${LEBINIOU_SHAREDIR}/www"
LEBINIOU_VUEDIR="${LEBINIOU_SHAREDIR}/vue"
LEBINIOU_SCHEMES_FILE="${LEBINIOU_SHAREDIR}/etc/schemes.json"

AC_SUBST(LEBINIOU_DATADIR)
AC_SUBST(LEBINIOU_PLUGINSDIR)
AC_SUBST(LEBINIOU_SAMPLESDIR)
AC_SUBST(LEBINIOU_WWWDIR)
AC_SUBST(LEBINIOU_VUEDIR)
AC_SUBST(LEBINIOU_SCHEMES_FILE)

# Check for SwScale
PKG_CHECK_MODULES([SWSCALE], [libswscale], , [AC_MSG_ERROR([You must have libswscale-dev installed])])

# Check for avutils
PKG_CHECK_MODULES([AVUTIL], [libavutil], , [AC_MSG_ERROR([You must have libavutil-dev installed])])

# Check for avcodec
PKG_CHECK_MODULES([AVCODEC], [libavcodec], , [AC_MSG_ERROR([You must have libavcodec-dev installed])])

# Check for avformat
PKG_CHECK_MODULES([AVFORMAT], [libavformat], , [AC_MSG_ERROR([You must have libavformat-dev installed])])

# Keep a list of input/output plugins
INPUT_PLUGINS="oss"
AC_SUBST([INPUT_PLUGINS])
OUTPUT_PLUGINS="SDL2"
AC_SUBST([OUTPUT_PLUGINS])

# Compile test/debug plugins
AC_ARG_ENABLE([test-plugins],
AS_HELP_STRING([--enable-test-plugins],[build the test/debug plugins [default=no]]),,
enable_test_plugins="no")
AM_CONDITIONAL([WITH_TEST_PLUGINS], [test "x${enable_test_plugins}" = "xyes"])

# Check for Alsa
alsa_present="no"
AC_ARG_ENABLE([alsa],
AS_HELP_STRING([--enable-alsa],[build the ALSA input plugin [default=yes]]),,
enable_alsa="yes")
if test "x${enable_alsa}" = "xyes"; then
PKG_CHECK_MODULES([ALSA], alsa, alsa_present=yes, alsa_present=no)
fi
AM_CONDITIONAL([ALSA_PLUGIN], [test "x${alsa_present}" = "xyes"])
if test "x${alsa_present}" = "xyes"; then
INPUT_PLUGINS="$INPUT_PLUGINS,alsa"
fi

# Check for JACK Audio
jack_present="no"
AC_ARG_ENABLE([jackaudio],
AS_HELP_STRING([--enable-jackaudio],[build the JACK Audio input plugin [default=yes]]),,
enable_jackaudio="yes")
if test "x${enable_jackaudio}" = "xyes"; then
PKG_CHECK_MODULES([JACK], jack, jack_present=yes, jack_present=no)
fi
AM_CONDITIONAL([JACK_PLUGIN], [test "x${jack_present}" = "xyes"])
if test "x${jack_present}" = "xyes"; then
INPUT_PLUGINS="$INPUT_PLUGINS,jackaudio"
fi

# Check for libpulse
pulseaudio_present="no"
AC_ARG_ENABLE([pulseaudio],
AS_HELP_STRING([--enable-pulseaudio],[build the PulseAudio input plugin [default=yes]]),,
enable_pulseaudio="yes")
if test "x${enable_pulseaudio}" = "xyes"; then
PKG_CHECK_MODULES([PULSEAUDIO], libpulse-simple, pulseaudio_present=yes, pulseaudio_present=no)
fi
AM_CONDITIONAL([PULSEAUDIO_PLUGIN], [test "x${pulseaudio_present}" = "xyes"])
if test "x${pulseaudio_present}" = "xyes"; then
INPUT_PLUGINS="$INPUT_PLUGINS,pulseaudio"
fi

# Check for eSound
esd_present="no"
AC_ARG_ENABLE([esd],
AS_HELP_STRING([--enable-esd],[build the ESD input plugin [default=no]]),,
enable_esd="no")
if test "x${enable_esd}" = "xyes"; then
PKG_CHECK_MODULES([ESD], esound >= 0.2.36, esd_present=yes, esd_present=no)
fi
AM_CONDITIONAL([ESD_PLUGIN], [test "x${esd_present}" = "xyes"])
if test "x${esd_present}" = "xyes"; then
INPUT_PLUGINS="$INPUT_PLUGINS,esound"
fi

# Check for sndfile
AC_ARG_ENABLE([sndfile],
AS_HELP_STRING([--enable-sndfile],[build the SndFile input plugin [default=yes]]),,
enable_sndfile="yes")
if test "x${enable_sndfile}" = "xyes"; then
PKG_CHECK_MODULES([SNDFILE], sndfile, sndfile_present=yes, sndfile_present=no)
fi
AM_CONDITIONAL([SNDFILE_PLUGIN], [test "x${sndfile_present}" = "xyes"])
if test "x${sndfile_present}" = "xyes"; then
INPUT_PLUGINS="$INPUT_PLUGINS,sndfile"
fi

# Twip input plugin
AC_ARG_ENABLE([twip],
AS_HELP_STRING([--enable-twip],[build the Twip input plugin [default=yes]]),,
enable_twip="yes")
AM_CONDITIONAL([TWIP_PLUGIN], [test "x${enable_twip}" = "xyes"])
if test "x${enable_twip}" = "xyes"; then
INPUT_PLUGINS="$INPUT_PLUGINS,twip"
fi

# Check for libcaca
caca_present="no"
AC_ARG_ENABLE([caca],
AS_HELP_STRING([--enable-caca],[build the libcaca plugin [default=yes]]),,
enable_caca="yes")
if test "x${enable_caca}" = "xyes"; then
PKG_CHECK_MODULES([CACA], caca, caca_present=yes, caca_present=no)
fi
AM_CONDITIONAL([CACA_PLUGIN], [test "x${caca_present}" = "xyes"])
if test "x${caca_present}" = "xyes"; then
OUTPUT_PLUGINS="$OUTPUT_PLUGINS,caca"
fi

# Fixed buffers support
AC_ARG_ENABLE([fixed],
[AS_HELP_STRING([--enable-fixed="WIDTHxHEIGHT"],[use fixed-size video buffers [default=no]])],
[enable_fixed=$enableval],
[enable_fixed="no"])
if test "x${enable_fixed}" != "xno"; then
FIXED_W=${enableval%*x*}
FIXED_H=${enableval#*x*}
CPPFLAGS="-DFIXED -DWIDTH=$FIXED_W -DHEIGHT=$FIXED_H $CPPFLAGS"
fi

# Set webcam capture size
AC_ARG_ENABLE([camsize],
[AS_HELP_STRING([--enable-camsize="WIDTHxHEIGHT"],[set webcam capture size [default="640x480"]])],
[enable_camsize=$enableval],
[enable_camsize="no"])
CAP_W=640
CAP_H=480
if test "x${enable_camsize}" != "xno"; then
CAP_W=${enableval%*x*}
CAP_H=${enableval#*x*}
CPPFLAGS="-DCAPTURE_SET -DCAP_WIDTH=$CAP_W -DCAP_HEIGHT=$CAP_H $CPPFLAGS"
fi

# Enable unfinished plugins
AC_ARG_ENABLE([unfinished-plugins],
AS_HELP_STRING([--enable-unfinished-plugins],[build unfinished plugins [default=no]]),,
enable_unfinished_plugins="no")
AM_CONDITIONAL([WITH_UNFINISHED_PLUGINS], [test "x${enable_unfinished_plugins}" = "xyes"])

# Flatpak support
AC_ARG_ENABLE([flatpak],
AS_HELP_STRING([--enable-flatpak],[flatpak support [default=no]]),,
enable_flatpak="no")
if test "x${enable_flatpak}" != "xno"; then
CPPFLAGS="-DFLATPAK $CPPFLAGS"
fi

# Check for getopt / getopt_long
# XXX completely review this -- only needed for the main binary --oliv3
# FIXME eeeuh on check 2x ?! --oliv3
AC_CHECK_FUNCS([getopt_long], , AC_DEFINE([HAVE_GETOPT_LONG]))
HAVE_GETOPT_LONG="no"
GETOPT_LONG_LIBS=""
AC_CHECK_FUNC([getopt_long], [HAVE_GETOPT_LONG="yes"])
if test "x$HAVE_GETOPT_LONG" != "xyes" ; then
# FreeBSD has a gnugetopt library
AC_CHECK_LIB([gnugetopt], [getopt_long], [HAVE_GETOPT_LONG="yes"])
if test "x$HAVE_GETOPT_LONG" = "xyes"; then
GETOPT_LONG_LIBS="-lgnugetopt"
fi
fi
AC_SUBST(GETOPT_LONG_LIBS)
if test "x$HAVE_GETOPT_LONG" = "xyes"; then
AC_DEFINE([HAVE_GETOPT_LONG])
fi

# Checks for header files
AC_CHECK_HEADERS([ \
sys/mman.h \
poll.h \
sys/time.h \
sys/ioctl.h \
sys/stat.h \
sys/types.h \
stdio.h \
stdlib.h \
string.h \
unistd.h \
dirent.h \
errno.h \
fcntl.h \
dlfcn.h \
zlib.h \
complex.h \
pthread.h \
ctype.h \
stdint.h \
assert.h \
malloc.h \
math.h \
getopt.h \
time.h \
soundcard.h \
sys/soundcard.h \
sys/select.h
])

# Check for SDL2
DEFAULT_OUTPUT_PLUGIN=SDL2
PKG_CHECK_MODULES([SDL2],[sdl2],have_sdl2=yes,[AC_MSG_ERROR([You must have libsdl2-dev installed])])

# Check for SDL2_ttf
PKG_CHECK_MODULES([SDL2_ttf],[SDL2_ttf],have_sdl2_ttf=yes,[AC_MSG_ERROR([You must have libsdl2-ttf-dev installed])])

# Set default font path
# default: Debian/Ubuntu
OSD_FONT="/usr/share/fonts/truetype/freefont/FreeMono.ttf"
OSD_PTSIZE=16
if test x"${os_flavor}" = x"freebsd"; then
OSD_FONT="/usr/local/share/fonts/dejavu/DejaVuSansMono.ttf"
OSD_PTSIZE=14
fi
AC_SUBST(OSD_FONT)
AC_SUBST(OSD_PTSIZE)

# Check for OpenGL/GLU
PKG_CHECK_MODULES([GLU],[glu],[have_opengl="yes"],[have_opengl="no"])
AC_ARG_ENABLE([opengl],
AS_HELP_STRING([--enable-opengl],[enable OpenGL support [default=no]]),,
[enable_opengl="no"])
AM_CONDITIONAL([EXTRA_OPENGL], [test "x${enable_opengl}" = "xyes"])
if test "x${enable_opengl}" = "xyes" && test "x${have_opengl}" = "xyes"; then
CPPFLAGS="-DWITH_GL $CPPFLAGS"
LIBS="${GLU_LIBS} $LIBS"
OUTPUT_PLUGINS="$OUTPUT_PLUGINS,GL"
fi

# ffmpeg/RTMP/mp4
OUTPUT_PLUGINS="$OUTPUT_PLUGINS,RTMP,mp4"

# Webcam support
have_webcam="no"
AC_CHECK_HEADERS(linux/videodev2.h, have_webcam=yes)
AC_CHECK_HEADERS(sys/videoio.h, have_webcam=yes)

AC_DEFINE_UNQUOTED([DEFAULT_INPUT_PLUGIN], ["$DEFAULT_INPUT_PLUGIN"], [Default input plugin])
AC_DEFINE_UNQUOTED([DEFAULT_OUTPUT_PLUGIN], ["$DEFAULT_OUTPUT_PLUGIN"], [Default output plugin])
AC_SUBST(LIBS)

if test "x$os_flavor" = "xopenbsd"; then
CPPFLAGS="-DSDL_DISABLE_IMMINTRIN_H $CPPFLAGS"
fi

# armhf
CFLAGS="-fsigned-char $CFLAGS"

# ISO C 2011
CFLAGS="-std=c11 $CFLAGS"
CPPFLAGS="-D_GNU_SOURCE $CPPFLAGS"

# ------------------------------------------------------------------
# Options
# ------------------------------------------------------------------
# Debugging option
AC_ARG_ENABLE([debug],
AS_HELP_STRING([--enable-debug],[turn on debugging [default=no]]),,
enable_debug="no")
if test "x${enable_debug}" = "xyes"; then
CPPFLAGS="-DDEBUG $CPPFLAGS"
CFLAGS="-g -O0 $CFLAGS"
else
CPPFLAGS="-DNDEBUG $CPPFLAGS"
CFLAGS="-fomit-frame-pointer $CFLAGS"
fi

# ASan
AC_ARG_ENABLE([asan],
AS_HELP_STRING([--enable-asan],[use AddressSanitizer [default=no]]),,
enable_asan="no")
if test "x${enable_asan}" = "xyes"; then
CFLAGS="-fsanitize=address $CFLAGS"
LIBS="-lasan $LIBS"
fi

# Treat warnings as errors
AC_ARG_ENABLE([warnings],
AS_HELP_STRING([--enable-warnings],[treat warnings as errors [default=yes]]),,
enable_warnings="yes")
if test "x${enable_warnings}" = "xyes"; then
CFLAGS="-Wall -Werror $CFLAGS"
if test "x${os_flavor}" != "xopenbsd"; then
CFLAGS="-Wextra -Wno-unused-parameter $CFLAGS"
fi
if test "x${os_flavor}" = "xnetbsd"; then
CFLAGS="-Wno-old-style-declaration $CFLAGS"
fi
fi

# Disable calls to dlclose() (for debugging with valgrind)
AC_ARG_ENABLE([dlclose],
AS_HELP_STRING([--enable-dlclose], [call dlclose() when unloading plugins [default=yes]]),[call_dlclose=no],[call_dlclose=yes])
if test "x${call_dlclose}" = "xno"; then
CPPFLAGS="-DDISABLE_DLCLOSE $CPPFLAGS"
fi

# ------------------------------------------------------------------
# Hardening
# ------------------------------------------------------------------
AC_CHECK_PROG(DPKG_BUILDFLAGS_CHECK,dpkg-buildflags,yes)
if test "x${enable_debug}" != "xyes"; then
if test "x${DPKG_BUILDFLAGS_CHECK}" = "xyes"; then
CFLAGS="`dpkg-buildflags --get CFLAGS` $CFLAGS"
CPPFLAGS="`dpkg-buildflags --get CPPFLAGS` $CPPFLAGS"
LDFLAGS="`dpkg-buildflags --get LDFLAGS` $LDFLAGS"
else
CFLAGS="-fstack-protector-strong -Wformat -Werror=format-security $CFLAGS"
if test "x$os_flavor" != "xopenbsd"; then
CPPFLAGS="-Wdate-time $CPPFLAGS"
fi
LDFLAGS="-Wl,-z,relro $LDFLAGS"
fi
fi

# ------------------------------------------------------------------
# Fixed buffers optimizations
# ------------------------------------------------------------------
if test "x${enable_fixed}" != "xno"; then
CFLAGS="-O3 -funroll-all-loops $CFLAGS"
fi

# ------------------------------------------------------------------
# Plugin-specific stuff
# ------------------------------------------------------------------
if test "x${have_webcam}" = "xyes"; then
AC_ARG_ENABLE([webcam],
AS_HELP_STRING([--enable-webcam],[enable webcam support [default=yes]]),[enable_webcam=$enableval],
[enable_webcam="yes"])
else
enable_webcam="no"
fi
AM_CONDITIONAL([EXTRA_WEBCAM], [test "x${enable_webcam}" = "xyes"])
if test "x${enable_webcam}" = "xyes"; then
CPPFLAGS="-DWITH_WEBCAM $CPPFLAGS"
fi

# ------------------------------------------------------------------
# Output files
# ------------------------------------------------------------------
AC_CONFIG_HEADERS([config.h])
AC_CONFIG_FILES([

Makefile
etc/Makefile
images/Makefile
src/Makefile
src/defaults.h
www/Makefile
www/static/Makefile

plugins/Makefile
plugins/input/Makefile
plugins/main/Makefile
plugins/output/Makefile

plugins/input/alsa/Makefile
plugins/input/random/Makefile
plugins/input/esound/Makefile
plugins/input/oss/Makefile
plugins/input/pulseaudio/Makefile
plugins/input/sndfile/Makefile
plugins/input/twip/Makefile
plugins/input/jackaudio/Makefile

plugins/main/_params/Makefile
plugins/main/acid_drop/Makefile
plugins/main/bassline/Makefile
plugins/main/blur_chemical/Makefile
plugins/main/blur_cross/Makefile
plugins/main/blur_diagonal_1/Makefile
plugins/main/blur_diagonal_2/Makefile
plugins/main/blur_gaussian/Makefile
plugins/main/blur_horizontal/Makefile
plugins/main/blur_horizontal_2/Makefile
plugins/main/blur_horizontal_colrot/Makefile
plugins/main/blur_light/Makefile
plugins/main/blur_vertical/Makefile
plugins/main/broken_mirror/Makefile
plugins/main/cellular_venus/Makefile
plugins/main/clear/Makefile
plugins/main/color_cycle/Makefile
plugins/main/color_flashy/Makefile
plugins/main/critters/Makefile
plugins/main/big_half_wheel/Makefile
plugins/main/hurricane/Makefile
plugins/main/smoke/Makefile
plugins/main/space_in/Makefile
plugins/main/space_out/Makefile
plugins/main/spiral_effect/Makefile
plugins/main/roller_x/Makefile
plugins/main/roller_y/Makefile
plugins/main/drops/Makefile
plugins/main/edge_detect/Makefile
plugins/main/emboss/Makefile
plugins/main/fadeout/Makefile
plugins/main/fadeout_beat/Makefile
plugins/main/fadeout_mist/Makefile
plugins/main/fadeout_slow/Makefile
plugins/main/faders/Makefile
plugins/main/flow/Makefile
plugins/main/galaxy/Makefile
plugins/main/GLCube/Makefile
plugins/main/hodge/Makefile
plugins/main/image/Makefile
plugins/main/image_beat_1/Makefile
plugins/main/image_beat_2/Makefile
plugins/main/image_beat_3/Makefile
plugins/main/image_colrot/Makefile
plugins/main/image_colrot_beat/Makefile
plugins/main/image_dissolve/Makefile
plugins/main/image_drop/Makefile
plugins/main/image_squares/Makefile
plugins/main/image_squares_beat/Makefile
plugins/main/images_pulse/Makefile
plugins/main/infinity/Makefile
plugins/main/kaleidoscope/Makefile
plugins/main/life/Makefile
plugins/main/melt/Makefile
plugins/main/mosaic/Makefile
plugins/main/monitor/Makefile
plugins/main/spirals_nested/Makefile
plugins/main/spiral_archimedean_3d/Makefile
plugins/main/spiral_pulsing/Makefile
plugins/main/paint_drops/Makefile
plugins/main/poincare/Makefile
plugins/main/oscillo_polar/Makefile
plugins/main/pulse/Makefile
plugins/main/recurrence_plot/Makefile
plugins/main/reflector/Makefile
plugins/main/ripple/Makefile
plugins/main/oscillo_rotating/Makefile
plugins/main/rotors/Makefile
plugins/main/rotors_freq/Makefile
plugins/main/scroll_horizontal/Makefile
plugins/main/scroll_vertical/Makefile
plugins/main/sequence_fadeout/Makefile
plugins/main/sin_oscillo_1/Makefile
plugins/main/sin_oscillo_2/Makefile
plugins/main/snake/Makefile
plugins/main/snake_oscillo/Makefile
plugins/main/speaker/Makefile
plugins/main/spheres_pulse/Makefile
plugins/main/spectrogram/Makefile
plugins/main/spectrum/Makefile
plugins/main/spectrum_s_horizontal/Makefile
plugins/main/spectrum_s_vertical/Makefile
plugins/main/spirals/Makefile
plugins/main/splash/Makefile
plugins/main/swarm/Makefile
plugins/main/taquin/Makefile
plugins/main/touw_eiffel/Makefile
plugins/main/tunnel/Makefile
plugins/main/video/Makefile
plugins/main/warp/Makefile
plugins/main/gum_x/Makefile
plugins/main/mirror_bottom/Makefile
plugins/main/mirror_top/Makefile
plugins/main/oscillo_x/Makefile
plugins/main/scanline_x/Makefile
plugins/main/shaker_x/Makefile
plugins/main/swap_columns/Makefile
plugins/main/gum_y/Makefile
plugins/main/mirror_left/Makefile
plugins/main/mirror_right/Makefile
plugins/main/oscillo_y/Makefile
plugins/main/path/Makefile
plugins/main/path_freq/Makefile
plugins/main/path_oscillo/Makefile
plugins/main/path_oscillo_freq/Makefile
plugins/main/rotozoom/Makefile
plugins/main/scanline_y/Makefile
plugins/main/shaker_y/Makefile
plugins/main/swap_rows/Makefile
plugins/main/takens/Makefile

plugins/main/tv_1d/Makefile
plugins/main/tv_colrot/Makefile
plugins/main/tv_colrot_slow/Makefile
plugins/main/tv_colrot_beat/Makefile
plugins/main/tv_diff/Makefile
plugins/main/tv_diff2/Makefile
plugins/main/tv_diff3/Makefile
plugins/main/tv_diff4/Makefile
plugins/main/tv_diffbeat/Makefile
plugins/main/tv_fire/Makefile
plugins/main/tv_nervous/Makefile
plugins/main/tv_predator/Makefile
plugins/main/tv_quark/Makefile
plugins/main/tv_streak/Makefile
plugins/main/tv_webcam/Makefile

plugins/output/caca/Makefile
plugins/output/mp4/Makefile
plugins/output/SDL2/Makefile
plugins/output/GL/Makefile
plugins/output/RTMP/Makefile

plugins/main/test_beat_detection/Makefile

])

AC_CONFIG_FILES([lebiniou.1.head lebiniou.1.tail])

INPUT_PLUGINS="$INPUT_PLUGINS,random,NULL"
AC_DEFINE_UNQUOTED([INPUT_PLUGINS], ["$INPUT_PLUGINS"], [Available input plugins])
OUTPUT_PLUGINS="$OUTPUT_PLUGINS,NULL"
AC_DEFINE_UNQUOTED([OUTPUT_PLUGINS], ["$OUTPUT_PLUGINS"], [Available output plugins])

# Plugins compilation flags
PLUGIN_CFLAGS="-fPIC"
PLUGIN_LDFLAGS="-shared -fPIC -Wl,-z,defs -pthread -Wl,--no-as-needed"
PLUGIN_LDADD="-l:liblebiniou.so.0"

AC_SUBST([PLUGIN_CFLAGS])
AC_SUBST([PLUGIN_LDFLAGS])
AC_SUBST([PLUGIN_LDADD])

# Output files
AC_OUTPUT

# ------------------------------------------------------------------
# Configuration report
# ------------------------------------------------------------------
yn() {
if test "x$1" = "xyes"; then
echo -n "	@<:@*@:>@ ";
else
echo -n "	@<:@ @:>@ ";
fi
echo $2
}

echo "-REPORT-"
echo
echo "=== Le Biniou v${BINIOU_VERSION} ==="
echo
echo " * Build options:"
echo "        Target OS:                  ${target_os}"
echo "        OS family:                  ${os_family}"
echo "        OS flavor:                  ${os_flavor}"
echo "        Debian-based:               ${DPKG_BUILDFLAGS_CHECK}" 
echo
echo "        Installation prefix:        ${prefix}"
echo "        Preprocessor flags:         ${CPPFLAGS}"
echo "        Compiler flags:             ${CFLAGS}"
echo "        Linker flags:               ${LDFLAGS}"
echo "        Libraries:                  ${LIBS}"
echo "        Debug enabled:              ${enable_debug}"
echo "        Treat warnings as errors:   ${enable_warnings}"
echo "        Use dlclose():              ${call_dlclose}"
echo "        Plugin compiler flags:      ${PLUGIN_CFLAGS}"
echo "        Plugin linker flags:        ${PLUGIN_LDFLAGS}"
echo "        Plugin libraries:           ${PLUGIN_LDADD}"
echo
echo "        Debug/test plugins:         ${enable_test_plugins}"
echo "        Unfinished plugins:         ${enable_unfinished_plugins}"
if test "x${enable_fixed}" != "xno"; then
echo "        Fixed video buffers:        ${enable_fixed}"
fi
echo -n "        Webcam:                     ${enable_webcam}"
if test "x${enable_webcam}" != "xno"; then
echo " (capture at ${CAP_W}x${CAP_H})"
else
echo
fi
echo "        Flatpak support:            ${enable_flatpak}"
echo "        Minify .js files:           ${with_uglifyjs}"
echo "        Minify .css files:          ${with_cleancss}"
echo "        Minify .html files:         ${with_htmlmin}"

echo
echo " * Input plugins:"
yn yes "OSS"
yn ${jack_present} "JACK Audio"
if test "x${os_family}" = "xlinux"; then
yn ${alsa_present} "ALSA"
yn ${pulseaudio_present} "PulseAudio"
fi
yn ${esd_present} "eSound"
yn yes "/dev/urandom"
yn ${sndfile_present} "sndfile"
yn ${enable_twip} "twip"
echo "   (Default input plugin: "${DEFAULT_INPUT_PLUGIN}")"

echo
echo " * Output plugins:"
yn ${have_sdl2} "SDL2"
echo "   (Default font: "${OSD_FONT}", "${OSD_PTSIZE}"px)"
yn ${caca_present} "libcaca"
if test "x${enable_opengl}" = "xyes"; then
yn ${enable_opengl} "GL"
fi
yn yes "MP4 encoder"
yn yes "RTMP transport"
echo "   (Default output plugin: "${DEFAULT_OUTPUT_PLUGIN}")"
echo
