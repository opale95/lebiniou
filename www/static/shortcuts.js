/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function shortcutChange(what, s) {
    command(`CMD_${what}_USE_SHORTCUT_${s}`);
}


function shortcutStore(what, s) {
    command(`CMD_${what}_STORE_SHORTCUT_${s}`);
}


function shortcutClick(what, s) {
    if (window.event.shiftKey) {
        shortcutStore(what, s);
    } else {
        shortcutChange(what, s);
    }
}


function createShortcuts(_css, type, shortcuts) {
    const css = _css[0].toUpperCase() + _css.substring(1);
    $(`#lb${css}sShortcuts`).empty();
    for (let i = 1; i <= shortcuts.length; i++) {
        $(`#lb${css}sShortcuts`).append('<div>'
                                        + `<button id="lb${css}Shortcut${i}"`
                                        + ` onclick="shortcutClick(\'${type}\', ${i});"`
                                        + ` class="lb-button">${i}`
                                        + '</button>'
                                        + '</div>');
        if (shortcuts[i - 1]) {
            $(`#lb${css}Shortcut${i}`).css({ "color": "orange" });
        } else {
            $(`#lb${css}Shortcut${i}`).css({ "color": "lime" });
        }
    }
}


function shortcutsCommandResult(r) {
    if (r.command) {
        if (r.command.startsWith("CMD_COL_STORE_SHORTCUT_")) {
            const shortcut = Number(r.command.replace(/^CMD_COL_STORE_SHORTCUT_/, ''));
            $(`#lbColormapShortcut${shortcut}`).css({ "color": "orange" });
            return 1;
        }

        if (r.command.startsWith("CMD_IMG_STORE_SHORTCUT_")) {
            const shortcut = Number(r.command.replace(/^CMD_IMG_STORE_SHORTCUT_/, ''));
            $(`#lbImageShortcut${shortcut}`).css({ "color": "orange" });
            return 1;
        }

        if (r.command.startsWith("CMD_COL_USE_SHORTCUT_")) {
            if (r.result.colormap) {
                $('#lbColormap').html(r.result.colormap);
                setColormap(r.result.colormap);
            }
            return 1;
        }

        if (r.command.startsWith("CMD_IMG_USE_SHORTCUT_")) {
            if (r.result.image) {
                $('#lbImage').html(r.result.image);
                setImage(r.result.image);
            }
            return 1;
        }
    }

    if (r.vuiCommand === 'VUI_SHORTCUT') {
        // console.log(colormap, image, index, id, cleared);
        let { colormap, image, index, id, cleared } = r.result;

        if (cleared === 'colormap') {
            $(`#lb$ColormapShortcut${index + 1}`).css({ "color": "lime" });
        } else if (cleared === 'image') {
            $(`#lb$ImageShortcut${index + 1}`).css({ "color": "lime" });
        } else if (colormap) {
            $(`#lbColormapShortcut${index + 1}`).css({ "color": "orange" });
        } else if (image) {
            $(`#lbImageShortcut${index + 1}`).css({ "color": "orange" });
        }
        return 1;
    }

    return 0;
}
