/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";


function threedCommandResult(r) {
    // console.log("threedCommandResult:", r);
    switch (r.command) {
    case "CMD_APP_NEXT_3D_BOUNDARY":
        setBoundary(r.result.boundary);
        return 1;

    case "CMD_APP_RANDOMIZE_3D_ROTATIONS":
    case "CMD_APP_TOGGLE_3D_ROTATIONS":
        setRotations(r.result);
        return 1;

    case "CMD_APP_DEC_3D_SCALE":
    case "CMD_APP_INC_3D_SCALE":
        return 1;
    }
    
    switch (r.uiCommand) {
    case "UI_CMD_TRACKBALL_ON_DRAG_START":
    case "UI_CMD_TRACKBALL_ON_DRAG_MOVE":
    case "UI_CMD_TRACKBALL_ON_MOUSE_WHEEL":
       return 1;

    case "UI_CMD_CONNECT":
        setBoundary(r.result.params3d.boundary);
        $('#lbRotationAmountSlider').slider({
            min: 0.0005,
            max: 0.01,
            value: r.result.params3d.rotateAmount,
            step: 0.0001,
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_3D_ROTATION_AMOUNT', ui.value, true);
                $('#lbRotationAmount').html(Math.floor(ui.value * 10000) / 10000);
            }
        });
        $('#lbRotationX').html(r.result.params3d.rotateFactor[0]);
        $('#lbRotationFactorXSlider').slider({
            min: -r.result.rotationFactor,
            max:  r.result.rotationFactor,
            value: r.result.params3d.rotateFactor[0],
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_3D_ROTATION_FACTOR', { axis: 'x', value: ui.value }, true);
                $('#lbRotationX').html(ui.value);
            }
        });
        $('#lbRotationY').html(r.result.params3d.rotateFactor[1]);
        $('#lbRotationFactorYSlider').slider({
            min: -r.result.rotationFactor,
            max:  r.result.rotationFactor,
            value: r.result.params3d.rotateFactor[1],
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_3D_ROTATION_FACTOR', { axis: 'y', value: ui.value }, true);
                $('#lbRotationY').html(ui.value);
            }
        });
        $('#lbRotationZ').html(r.result.params3d.rotateFactor[2]);
        $('#lbRotationFactorZSlider').slider({
            min: -r.result.rotationFactor,
            max:  r.result.rotationFactor,
            value: r.result.params3d.rotateFactor[2],
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_3D_ROTATION_FACTOR', { axis: 'z', value: ui.value }, true);
                $('#lbRotationZ').html(ui.value);
            }
        });
        setRotations(r.result.params3d);
        return 1;

    case "UI_CMD_APP_SET_3D_ROTATION_AMOUNT":
        setRotations(r.result);
        return 1;

    case "UI_CMD_APP_SET_3D_ROTATION_FACTOR":
        $('#lbRotationX').html(r.result.rotateFactor[0]);
        $('#lbRotationFactorXSlider').slider({ values: [ r.result.rotateFactor[0] ] });
        $('#lbRotationY').html(r.result.rotateFactor[1]);
        $('#lbRotationFactorYSlider').slider({ values: [ r.result.rotateFactor[1] ] });
        $('#lbRotationZ').html(r.result.rotateFactor[2]);
        $('#lbRotationFactorZSlider').slider({ values: [ r.result.rotateFactor[2] ] });
        return 1;

    case "UI_CMD_SELECT_ITEM":
        if (r.result.item === "boundaryMode") {
            setBoundary(r.result.mode);
        }
        return 1;
    }
    
    return 0;
}


function setRotations(rotations) {
    const a = rotations.rotateFactor;

    if (a[0] || a[1] || a[2]) {
        $('#lbRotationsCheckbox').prop('checked', true);
    } else {
        $('#lbRotationsCheckbox').prop('checked', false);
    }
    $('#lbRotationFactorXSlider').slider({ values: [ a[0] ] });
    $('#lbRotationFactorYSlider').slider({ values: [ a[1] ] });
    $('#lbRotationFactorZSlider').slider({ values: [ a[2] ] });
    $('#lbRotationX').html(a[0]);
    $('#lbRotationY').html(a[1]);
    $('#lbRotationZ').html(a[2]);
    $('#lbRotationAmountSlider').slider({ values: [ rotations.rotateAmount ] });
    $('#lbRotationAmount').html(Math.floor(rotations.rotateAmount * 10000) / 10000);
}


function setBoundary(b) {
    switch (b) {
    case "none":
    case "cube":
    case "sphere_dots":
    case "sphere_wireframe":
        $('#lbBoundarySelect').val(b);
        break;
    }
}


function threedEvent(e) {
    // console.error("threedEvent:", e);
    switch (e.event) {
    case "onDragStart":
        uiCommand('UI_CMD_TRACKBALL_ON_DRAG_START', e.pos);
        break;

    case "onDragMove":
        uiCommand('UI_CMD_TRACKBALL_ON_DRAG_MOVE', e.pos);
        break;

    case "onMouseWheel":
        uiCommand('UI_CMD_TRACKBALL_ON_MOUSE_WHEEL', e.wheel);
        break;
    }
}


function threedInit() {
    // console.log("threedInit");
    $('#lbTrackballLighting').hide();
    if (PhiloGL.hasWebGL()) {
        if (lbPrefs.displayThreed) {
            webGLStart();
        } else {
            $('#lbThreed').hide();
        }
    } else {
        $('#lbThreedRow').hide();
    }
}


function toggleLighting() {
    $('#lbTrackballLighting').toggle();
}
