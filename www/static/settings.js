/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let gWidth, gHeight;
let gJackaudioLeft = "system:capture_1", gJackaudioRight = "system:capture_2";
let gWebcams, gMaxCams;
let pluginsSelect, themesSelect;


function start() {
    console.info("Entering settings");
    getSettings();
}


function getSettings() {
    fetch('/settings').then(response => response.json()).then(settings => {
        console.info(settings);
        setVersion(settings.version);
        setConfigurationFile(settings.file);
        setInput(settings.input);
        setScreen(settings.screen);
        setPlugins(settings.plugins);
        setThemes(settings.themes);
        setEngine(settings.engine);
        setMisc(settings.misc);
    });
}


function setVersion(version) {
    $('#lbVersion').val(version);
    lbVersion = version;
}


function setConfigurationFile(file) {
    if (!file.includes(".json")) {
        file += ".json";
    }
    $('#lbConfigurationFile').html(file);
    $('#lbFile').val(file);
}


function setInput(input) {
    const { name, allInputPlugins, volumeScale } = input;
    $('#lbVolumeScale').val(volumeScale.toFixed(1));
    if (name !== "jackaudio") {
        $('.lb-jackaudio-container').hide();
    }
    let select = '<b class="lb-setting">Input:&nbsp;</b><select id="lbInputName" name="name" onchange="inputChange(this.value);">';
    for (let i in allInputPlugins) {
        if (![ "NULL", "sndfile" ].includes(allInputPlugins[i])) {
            select += `<option value="${allInputPlugins[i]}"`;
            select += (allInputPlugins[i] === name) ? ' selected' : '';
            select += `>${allInputPlugins[i]}</option>`;
        }
    }
    select += '</select>';
    $('#lbInput').html(select);
    if (name === "jackaudio") {
        const { jackaudioLeft, jackaudioRight } = input;
        gJackaudioLeft = jackaudioLeft;
        gJackaudioRight = jackaudioRight;
        $('#lbJackaudioLeft').val(jackaudioLeft);
        $('#lbJackaudioRight').val(jackaudioRight);
    }
}


function inputChange(name) {
    if (name === "jackaudio") {
        $('.lb-jackaudio-container').show();
        $('#lbJackaudioLeft').val(gJackaudioLeft);
        $('#lbJackaudioRight').val(gJackaudioRight);
    } else {
        $('.lb-jackaudio-container').hide();
    }
}


const SCREEN_RESOLUTIONS_NAMES = {
    '640x360': 'nHD',
    '800x600': 'SVGA',
    '960x540': 'qHD',
    '1024x768': 'XGA',
    '1280x720': 'WXGA',
    '1280x1024': 'SXGA',
    '1366x768': 'HD',
    '1440x900': 'WXGA+',
    '1600x900': 'HD+',
    '1680x1050': 'WSXGA+',
    '1920x1080': 'FHD',
    '1920x1200': 'WUXGA',
    '2048x1152': 'QWXGA',
    '2560x1440': 'QHD',
    '3840x2160': '4K UHD'
}

const SCREEN_RESOLUTIONS = {
    'nHD': [ 640, 360 ],
    'SVGA': [ 800, 600 ],
    'qHD': [ 960, 540 ],
    'XGA': [ 1024, 768 ],
    'WXGA': [ 1280, 720 ],
    'SXGA': [ 1280, 1024 ],
    'HD': [ 1366, 768 ],
    'WXGA+': [ 1440, 900 ],
    'HD+': [ 1600, 900 ],
    'WSXGA+': [ 1680, 1050 ],
    'FHD': [ 1920, 1080 ],
    'WUXGA': [ 1920, 1200 ],
    'QWXGA': [ 2048, 1152 ],
    'QHD': [ 2560, 1440 ],
    '4K UHD': [ 3840, 2160 ]
}


function setScreen(screen) {
    const { width, height } = screen;
    gWidth = width;
    gHeight = height;
    $('#lbWidthInput').val(width);
    $('#lbHeightInput').val(height);
    if (screen.fixed) {
        $('.screen-container').hide();
    } else {
        buildScreenSelect(screen);
    }
}

function setResolution() {
    const screen = {
        "width": $('#lbWidthInput').val(),
        "height": $('#lbHeightInput').val()
    }
    buildScreenSelect(screen);
}


function buildScreenSelect(screen) {
    const { width, height } = screen;
    $('#lbWidthInput').val(width);
    $('#lbHeightInput').val(height);
    const name = SCREEN_RESOLUTIONS_NAMES[width + 'x' + height];
    const option = name ? name : 'custom';
    let select = '<b class="lb-setting">Display:&nbsp;</b><select onchange="resolutionChange(this.value);">';
    for (const i in SCREEN_RESOLUTIONS) {
        const [ w, h ] = SCREEN_RESOLUTIONS[i];
        select += `<option name="${i}"`;
        select += (i === option) ? ' selected' : '';
        select += `>${i} (${w}x${h})</option>`;
    }
    select += '<option name="custom"';
    select += (option === 'custom') ? ' selected' : '';
    select += '>Custom</option>';
    select += '</select>';
    $('#lbResolution').html(select);
}


function resolutionChange(s) {
    if (s !== 'Custom') {
        const [ w, h ] = s.split('(')[1].split(')')[0].split('x');
        buildScreenSelect({ width: w, height: h });
    } else {
        buildScreenSelect({ width: gWidth, height: gHeight });
    }
}


function setPlugins(plugins) {
    plugins.sort((a, b) => b.name < a.name);
    let select = '<b class="lb-setting">Plugins:&nbsp;</b><select id="lbPluginsSelect" name="plugins" multiple>';
    for (let i in plugins) {
        const p = plugins[i];
        select += `<option value="${p.name}"`;
        select += p.enabled ? ' selected' : '';
        select += `>${p.displayName}</option>`;
    }
    select += '</select>';
    $('#lbPlugins').html(select);
    pluginsSelect = new vanillaSelectBox("#lbPluginsSelect", {
        search: true
    });
}


function setThemes(themes) {
    const { baseThemes, selectedThemes, userThemes } = themes;
    let options = [];
    baseThemes.map(t => options.push({ "value": t, "name": t}));
    userThemes.map(t => options.push({ "value": '~' + t, "name": t}));
    options.sort((a, b) => b.name < a.name);
    let select = '<b class="lb-setting">Themes:&nbsp;</b><select id="lbThemes" name="themes" multiple>';
    for (const i in options) {
        const o = options[i];
        select += `<option value="${o.value}"`;
        select += selectedThemes.includes(o.value) ? ' selected' : '';
        select += `>${o.name}</option>`;
    }
    select += '</select>';
    select += '<p>Add more themes by putting images in any <span class="formatted">~/.lebiniou/images/</span> subdirectory. ';
    select += 'Example: <span class="formatted"><span id="lbHomeDir"></span>/.lebiniou/images/my_cool_theme/my_cool_image_1.png</span>';
    select += ' will add <span class="formatted">my_cool_theme</span> to the list.</p>';
    $('#lbThemesContainer').html(select);
    themesSelect = new vanillaSelectBox("#lbThemes", {
        search: true
    });
}


function setEngine(engine) {
    const {
        startMode,
        autoColormapsMode, colormapsMin, colormapsMax,
        autoImagesMode, imagesMin, imagesMax,
        webcams, autoWebcamsMode, webcamsMin, webcamsMax, maxCams, hFlip, vFlip,
        autoSequencesMode, sequencesMin, sequencesMax,
        randomMode, maxFps, fadeDelay,
        flatpak
    } = engine;

    $('#lbStartMode').val(startMode);
    $('#lbHFlip').prop("checked", hFlip);
    $('#lbVFlip').prop("checked", vFlip);
    $('#lbFlatpak').val(flatpak);
    if (flatpak) {
        $('#lbCommand').html("flatpak run net.biniou.LeBiniou");
    } else {
        $('#lbCommand').html("lebiniou");
    }

    $('#lbMaxFps').val(maxFps);
    $('#lbFadeDelay').val(fadeDelay);
    $('#lbRandomMode').val(randomMode);
    $('#lbAutoSequencesMode').val(autoSequencesMode);
    $('#lbAutoColormapsMode').val(autoColormapsMode);
    $('#lbAutoImagesMode').val(autoImagesMode);
    $('#lbAutoWebcamsMode').val(autoWebcamsMode);
    $('#lbSequencesMin').val(sequencesMin);
    $('#lbSequencesMax').val(sequencesMax);
    $('#lbColormapsMin').val(colormapsMin);
    $('#lbColormapsMax').val(colormapsMax);
    $('#lbImagesMin').val(imagesMin);
    $('#lbImagesMax').val(imagesMax);
    gWebcams = webcams;
    gMaxCams = maxCams;
    $('#lbWebcams').val(webcams);
    $('#lbWebcams').prop("max", maxCams);
    $('#lbWebcamsMin').val(webcamsMin);
    $('#lbWebcamsMax').val(webcamsMax);
    if (webcams < 2) {
        $('#lbAutoWebcamsRow').hide();
    } else {
        $('#lbAutoWebcamsRow').show();
    }
}


function postForm() {
    if (!checkRange("#lbMaxFps", 1, 255)) return;
    if (!checkRange("#lbFadeDelay", 1, 255)) return;
    if (!checkRange("#lbWebcams", 0, gMaxCams)) return;
    if (!checkMinMax("#lbSequencesMin", "#lbSequencesMax")) return;
    if (!checkMinMax("#lbColormapsMin", "#lbColormapsMax")) return;
    if (!checkMinMax("#lbImagesMin", "#lbImagesMax")) return;
    if ((gWebcams > 1) && !checkMinMax("#lbWebcamsMin", "#lbWebcamsMax")) return;
    if (!checkVolumeScale()) return;
    const form = {
        createSymlink: $('#lbCreateSymlink').prop('checked'),
        version: Number($('#lbVersion').val()),
        screen: {
            width: Number($('#lbWidthInput').val()),
            height: Number($('#lbHeightInput').val())
        },
        input: {
            name: $('#lbInputName').val(),
            volumeScale: Number($('#lbVolumeScale').val()),
            jackaudioLeft: $('#lbJackaudioLeft').val(),
            jackaudioRight: $('#lbJackaudioRight').val()
        },
        themes: $('#lbThemes').val(),
        engine: {
            hFlip: $('#lbHFlip').prop('checked'),
            vFlip: $('#lbVFlip').prop('checked'),
            maxFps: Number($('#lbMaxFps').val()),
            fadeDelay: Number($('#lbFadeDelay').val()),
            startMode: $('#lbStartMode').val(),
            randomMode: Number($('#lbRandomMode').val()),
            autoSequencesMode: $('#lbAutoSequencesMode').val(),
            sequencesMin: Number($('#lbSequencesMin').val()),
            sequencesMax: Number($('#lbSequencesMax').val()),
            autoColormapsMode: $('#lbAutoColormapsMode').val(),
            colormapsMin: Number($('#lbColormapsMin').val()),
            colormapsMax: Number($('#lbColormapsMax').val()),
            autoImagesMode: $('#lbAutoImagesMode').val(),
            imagesMin: Number($('#lbImagesMin').val()),
            imagesMax: Number($('#lbImagesMax').val()),
            autoWebcamsMode: $('#lbAutoWebcamsMode').val(),
            webcamsMin: Number($('#lbWebcamsMin').val()),
            webcamsMax: Number($('#lbWebcamsMax').val()),
            webcams: Number($('#lbWebcams').val())
        },
        plugins: $('#lbPluginsSelect').val(),
        statistics: $('#lbStatistics').prop('checked')
    }
    fetch('/settings.html', {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(form)
    }).then(data  => data.json()).then(res => {
        console.log(res);
        if (res.restart) {
            alert("You need to restart Le Biniou for the changes to take effect.");
        }
    });
}


function checkRange(field, min, max) {
    const val = +$(field).val();

    if (val < min) {
        alert(`${val} must be >= ${min}`);
        return 0;
    } else if (val > max) {
        alert(`${val} must be <= ${max}`);
        return 0;
    } else {
        return 1;
    }
}

function checkMinMax(min, max) {
    const minVal = +$(min).val();
    const maxVal = +$(max).val();

    if (minVal < 1) {
        alert(`${min} must be >= 1`);
        return 0;
    }
    if (maxVal < 1) {
        alert(`${max} must be >= 1`);
        return 0;
    }
    if (minVal > 60) {
        alert(`${min} must be <= 60`);
        return 0;
    }
    if (maxVal > 60) {
        alert(`${max} must be <= 1`);
        return 0;
    }
    if (minVal > maxVal) {
        alert(`${min} must be <= ${max}`);
        return 0;
    }

    return 1;
}


function setMisc(misc) {
    const { homeDir, statistics, desktopSymlink } = misc;
    $('#lbHomeDir').html(homeDir);
    $('#lbStatistics').prop("checked", statistics);
    if (desktopSymlink) {
        $('#lbSymlink').hide();
        $('#lbCreateSymlink').prop("checked", false);
    }
}


function checkVolumeScale() {
    if ($('#lbVolumeScale').val() <= 0) {
        alert("Volume scale must be > 0");
        return 0;
    }
    return 1;
}
