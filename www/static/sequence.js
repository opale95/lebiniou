/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

function sequenceCommandResult(r) {
    switch (r.command) {
    case "CMD_APP_FIRST_SEQUENCE":
    case "CMD_APP_LAST_SEQUENCE":
    case "CMD_APP_NEXT_SEQUENCE":
    case "CMD_APP_PREVIOUS_SEQUENCE":
    case "CMD_APP_RANDOM_SCHEME":
    case "CMD_APP_RANDOM_SEQUENCE":
    case "CMD_SEQ_LAYER_DEFAULT":
    case "CMD_SEQ_LAYER_NEXT":
    case "CMD_SEQ_LAYER_PREVIOUS":
    case "CMD_SEQ_MOVE_DOWN":
    case "CMD_SEQ_MOVE_UP":
    case "CMD_SEQ_TOGGLE_LENS":
    case "CMD_APP_TOGGLE_SELECTED_PLUGIN":
        if (r.result.sequenceName) {
            setSequenceName(r.result.sequenceName);
        }
        setSequence(r.result.sequence);
        return 1;

    case "CMD_SEQ_PARAM_DEC":
    case "CMD_SEQ_PARAM_DEC_FAST":
    case "CMD_SEQ_PARAM_INC":
    case "CMD_SEQ_PARAM_INC_FAST":
        parameters(r.result);
        return 1;

    case "CMD_SEQ_PARAM_PREVIOUS":
    case "CMD_SEQ_PARAM_NEXT":
        return 1;

    case "CMD_SEQ_SAVE_BARE":
    case "CMD_SEQ_SAVE_FULL":
    case "CMD_SEQ_UPDATE_BARE":
    case "CMD_SEQ_UPDATE_FULL":
        const { sequenceName, sequence } = r.result;
        setSequenceName(sequenceName);
        setSequence(sequence);
        return 1;

    case "CMD_SEQ_PARAM_NEXT":
    case "CMD_SEQ_PARAM_SELECT_PREVIOUS":
    case "CMD_SEQ_PARAM_SELECT_NEXT":
        return 1;
    }

    switch (r.uiCommand) {
    case "UI_CMD_CONNECT":
    case "UI_CMD_SEQ_REORDER":
        setSequenceName(r.result.sequenceName);
        setSequence(r.result.sequence);
        return 1;

    case "UI_CMD_SEQ_SET_LAYER_MODE":
        setSequence(r.result.sequence);
        return 1;

    case "UI_CMD_SEQ_RENAME":
        if (r.result.error) {
            alert('Renaming failed: ' + r.result.error);
        }
        setSequenceName(r.result.sequenceName);
        return 1;
    }

    return 0;
}


function sequenceInit() {
    $("#lbSequence").sortable({
        update: function(event, ui) {
            const ul = $('#lbSequence').children();
            let newSequence = [];
            for (let i = 0; i < ul.length; ++i) {
                newSequence.push(ul[i].getAttribute('name'));
            }
            uiCommand('UI_CMD_SEQ_REORDER', newSequence);
        }
    });
}


function layerModeSelect(plugin) {
    let html = `<select class="lb-layer-mode" onchange="uiCommand(\'UI_CMD_APP_SELECT_PLUGIN\', \'${plugin.name}\'); uiCommand(\'UI_CMD_SEQ_SET_LAYER_MODE\', { \'plugin\': \'${plugin.name}\', \'mode\': +this.value });">`;

    for (const layer in layerModes) {
        html += `<option value="${layer}"`;
        if (plugin.mode === layerModes[layer]) {
            html += ' selected';
        }
        html += `>${fmt(layerModes[layer])}</option>`;
    }
    html += '</select>';

    return html;
}


function setSequence(s) {
    // console.log("setSequence", s);
    const {
        sequenceName,
        autoColormaps, autoImages,
        colormap, image,
        params3d, plugins,
        bandpassMin, bandpassMax
    } = s;
    let lensFound = false;

    if (sequenceName) {
        setSequenceName(sequenceName);
    }
    $('#lbSequence').empty();
    for (const i in plugins) {
        const p = plugins[i];
        let li = `<li name="${p.name}"><button class="lb-plugin" onclick="uiCommand(\'UI_CMD_APP_SELECT_PLUGIN\', \'${p.name}\');">${p.displayName}</button>${layerModeSelect(p)}<button class="lb-lens" onclick="uiCommand(\'UI_CMD_APP_SELECT_PLUGIN\', \'${p.name}\'); command(\'CMD_SEQ_TOGGLE_LENS\');">`;
        if (p.lens) {
            lensFound = true;
            li += '&#x21aa;';
        } else if (!lensFound) {
            li += '&#x2193;';
        } else {
            li += '&nbsp;';
        }
        li += '</button>';
        li += '</li>';
        $('#lbSequence').append(li);

        if (p.name === selectedPlugin) {
            parameters(p);
        }
    }

    autoModeResult("colormaps", autoColormaps);
    $('#lbColormap').html(colormap);
    setColormap(colormap);

    autoModeResult("images", autoImages);
    $('#lbImage').html(image.split('.')[0]);
    setImage(image);

    if (params3d) {
        setRotations(params3d);
    }

    $('#lbBandpassRange').html(`${bandpassMin} - ${bandpassMax}`);
    $('#lbBandpassSlider').slider({ values: [ bandpassMin, bandpassMax ] });
}


function saveSequence() {
    if (window.event.shiftKey) {
        command('CMD_SEQ_SAVE_BARE');
    } else {
        command('CMD_SEQ_SAVE_FULL');
    }
}


function updateSequence() {
    if (window.event.shiftKey) {
        command('CMD_SEQ_UPDATE_BARE');
    } else {
        command('CMD_SEQ_UPDATE_FULL');
    }
}


function resetSequence() {
    command('CMD_SEQ_RESET');
    if (window.event.ctrlKey) {
        command('CMD_APP_CLEAR_SCREEN');
    }
    if (window.event.shiftKey) {
        command('CMD_APP_RANDOMIZE_SCREEN');
    }
}


const setSequenceName = name => $('#lbSequenceName').val(name);
