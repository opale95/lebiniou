/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

const TABS = 9;
const OPTIONS = [
    "BO_SFX",
    "BO_GFX",
    "BO_IMAGE",
    "BO_BLUR",
    "BO_DISPLACE",
    "BO_WARP",
    "BO_LENS",
    "BO_WEBCAM",
    null,
    null
];
let tab = 0;


function selectTab(idx) {
    tab = idx;
    $(`#lbPluginsTab${idx}`).css({
        "color": "blue",
        "background-color": "black"
    });
}


function unselectTab(idx) {
    $(`#lbPluginsTab${idx}`).css({
        "color": "white",
        "background-color": "black"
    });
}


function activateTab(idx) {
    for (let i in OPTIONS) {
        unselectTab(i);
    }
    selectTab(idx);
    const query = (OPTIONS[idx] ? `?option=${OPTIONS[idx]}` : '');
    fetch(`/plugins${query}`).then(response => response.json()).then(plugins => {
        $('#lbPlugins').empty();
        for (let i in plugins) {
            const { name, displayName, favorite } = plugins[i];

            if (((tab === 9) && favorite) || (tab < 9)) {
                $('#lbPlugins').append(`<button id="lbPlugin${i}" class="lb-plugin lb-button" onclick="selectPlugin(\'${name}\');" title="Click to select ${displayName}, Ctrl+Click to add/remove">${displayName}</button>`);
                if (plugins[i].selected) {
                    $(`#lbPlugin${i}`).css({
                        "color": "black",
                        "background-color": "lime"
                    });
                }
            }
        }
    });
}


function pluginsInit() {
    activateTab(tab);
}


function selectPlugin(p) {
    uiCommand('UI_CMD_APP_SELECT_PLUGIN', p);
    if (window.event.ctrlKey) {
        command('CMD_APP_TOGGLE_SELECTED_PLUGIN');
    }
}


function pluginsCommandResult(r) {
    switch (r.command) {
    case "CMD_PLG_PREVIOUS":
    case "CMD_PLG_NEXT":
    case "CMD_PLG_SCROLL_DOWN":
    case "CMD_PLG_SCROLL_UP":
        return 1;

    case "UI_CMD_CONNECT":
        pluginsInit(r.result.selectedPlugin);
        return 1;

    }

    return 0;
}
