/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";


function pluginCommandResult(r) {
    // console.log("plugin_command_result:", r);
    switch (r.command) {
    case "CMD_SEQ_SELECT_NEXT":
    case "CMD_SEQ_SELECT_PREVIOUS":
    case "CMD_PLG_NEXT":
    case "CMD_PLG_PREVIOUS":
    case "CMD_PLG_SCROLL_DOWN":
    case "CMD_PLG_SCROLL_UP":
        selectedPlugin = r.result.selectedPlugin;
        $('#lbSelectedPluginDname').html(r.result.selectedPluginDname);
        parameters(r.result);
        return 1;

    case "CMD_APP_TOGGLE_SELECTED_PLUGIN":
        setSequence(r.result.sequence);
        return 1;

    case "CMD_APP_LOCK_SELECTED_PLUGIN":
        setLocked(r.result.lockedPlugin);
        return 1;

    case "UI_CMD_CONNECT":
        if (r.result.lockedPlugin) {
            setLocked(r.result.selectedPlugin === r.result.lockedPlugin);
        } else {
            setLocked(false);
        }
        return 1;
    }

    switch(r.uiCommand) {
    case "UI_CMD_APP_SELECT_PLUGIN":
        parameters(r.result);
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE":
        parameters(r.result);
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE":
        $('#lbParameterValue' + r.result.selectedParam).html(formatPlaylist(r.result.value));
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_SELECT_VALUE":
        $('#lbParamSelect' + r.result.selectedParam).val(r.result.value);
        return 1;

    case "UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE":
        $(`#lbParamSlider${r.result.selectedParam}`).slider('value', r.result.value);
        if (r.result.type === "integer") {
            $(`#lbParameterValue${r.result.selectedParam}`).html(r.result.value);
        } else { // double
            $(`#lbParameterValue${r.result.selectedParam}`).html(r.result.value / 1000);
        }
        return 1;
    }

    return 0;
}


function fmt(s) {
    s = s.charAt(0).toUpperCase() + s.slice(1);

    return s.replace(/_/g, ' ');
}


function parameters(p) {
    if (p.parameters) {
        const params = p.parameters;

        if (params) {
            let html = '';
            const keys = Object.keys(params);

            $('#lbParameters').empty();
            for (let i in keys) {
                const key = keys[i];
                const param = params[key];

                html += parameterHtml('lbParam', i, key, param);
            }
            $('#lbParameters').append(html);
            $('#lbParameters').show();

            for (let i in keys) {
                const key = keys[i];
                const param = params[key];

                if ((param.type === "integer") || (param.type === "double")) {
                    const isDouble = (param.type === "double");
                    $(`#lbParamSlider${i}`).slider({
                        min: isDouble ? param.min * 1000 : param.min,
                        max: isDouble ? param.max * 1000 : param.max,
                        step: isDouble ? param.step * 1000 : param.step,
                        value: isDouble ? param.value * 1000 : param.value,
                        animate: "fast",
                        orientation: "horizontal",
                        slide: function(event, ui) {
                            let id = $(this).attr('id');
                            id = id.replace(/^lbParamSlider/, '');
                            uiCommand('UI_CMD_SEQ_SET_PARAM_SLIDER_VALUE', { 'selectedParam': Number(id), 'value': ui.value }, true);
                            if (isDouble) {
                                $(`#lbParameterValue${id}`).html(ui.value / 1000);
                            } else { // integer
                                $(`#lbParameterValue${id}`).html(ui.value);
                            }
                        }
                    });
                } else if (param.type === "playlist") {
                    $(`#lbParamPlaylist${i}`).change(function(e) {
                        let id = $(this).attr('id');
                        id = id.replace(/^lbParamPlaylist/, '');
                        let playlist = [];
                        for (let i = 0; i < e.target.files.length; ++i) {
                            playlist.push(e.target.files[i].name);
                        }
                        uiCommand('UI_CMD_SEQ_SET_PARAM_PLAYLIST_VALUE', { 'selectedParam': + Number(id), 'value': playlist });
                    });
                    $(`#lbParamPlaylistValue${i}`).html(param.value);
                }
            }
        } else {
            $('#lbParameters').hide();
        }
    } else {
        $('#lbParameters').hide();
    }
}


function parameterHtml(prefix, index, name, param) {
    let html = '';

    if (param.type === "boolean") {
        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><input type="checkbox" id="${prefix}Checkbox${index}" class="lb-parameter-checkbox" onchange="uiCommand(\'UI_CMD_SEQ_SET_PARAM_CHECKBOX_VALUE\', { \'selectedParam\': ${+index}, \'value\': this.checked });" ${param.value ? ' checked' : ''}`;
        html += param.description ? ` title="${param.description}"` : '';
        html += ' /></div>';
    } else if ((param.type === "integer") || (param.type === "double")) {
        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><div id="lbParameterValue${index}" class="lb-parameter-value">${param.value}</div><div id="${prefix}Slider${index}"`;
        html += param.description ? ` title="${param.description}"` : '';
        html += '></div></div>';
    } else if (param.type === "string_list") {
        const valueList = param.value_list;

        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><select class="lb-string-list" id="${prefix}Select${index}" class="lb-parameter-string-list" onchange="uiCommand(\'UI_CMD_SEQ_SET_PARAM_SELECT_VALUE\', { \'selectedParam\': ${+index}, \'value\': this.value });"`;
        html += param.description ? ` title="${param.description}"` : '';
        html += '>';
        for (let j in valueList) {
            const selected = (param.value === valueList[j]) ? ' selected': '';
            html += `<option value="${valueList[j]}" ${selected}>${valueList[j]}</option>`;
        }
        html += '</select></div>';
    } else if (param.type === "playlist") {
        html += `<div class="lb-parameter-name">${fmt(name)}</div><div><div id="lbParameterValue${index}" class="lb-parameter-value">${formatPlaylist(param.value)}</div><div><input type="file" id="${prefix}Playlist${index}" multiple`;
        html += param.description ? ` title="${param.description}"` : '';
        html += '></div></div>';
    }

    return html;
}

function changeParam(param, value) {
    uiCommand('UI_CMD_SEQ_SET_INTEGER_PARAM_VALUE', { "param": param, "value": +value });
}


function setLocked(p) {
    if (p) {
        $('#lbLocked').html('&#x1f512;');
    } else {
        $('#lbLocked').html('&#x1f513;');
    }
}


function formatPlaylist(p) {
    let html = "<ol>";

    for (let i in p) {
        html += `<li>${p[i]}</li>`;
    }

    html += "</ol>";
    return html;
}
