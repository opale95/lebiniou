/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let lbWs;
let lbWsUrl;
let lbHttpUrl;
let lbPrefs = { };
let layerModes = [];
let selectedPlugin = "";
let screenWidth = 0, screenHeight = 0;


function connect() {
    setHostPort();
    console.log(`Connecting to ${lbWsUrl}`);
    lbWs = new WebSocket(lbWsUrl);


    lbWs.onopen = function() {
        console.info("Connected");
        uiCommand('UI_CMD_CONNECT');
    };


    lbWs.onmessage = function(event) {
        // console.log("event.data", event.data);
        const payload = JSON.parse(event.data);
        // console.log("payload", payload);

        if (payload.ping) {
            // console.log("ping", payload.ping);
            // fps
            const fps = Math.min(payload.ping.fps, payload.ping.maxFps);
            const pct = `${Math.round(fps / payload.ping.maxFps * 100)}%`;
            $('#lbFps').html(payload.ping.fps);
            const elem = document.getElementById("lbFps");
            elem.style.width = pct;

            // uptime
            let uptime = payload.ping.uptime;
            const d = Math.floor(uptime / (3600*24));
            uptime -= d*3600*24;
            const h = Math.floor(uptime / 3600);
            uptime -= h*3600;
            const m = Math.floor(uptime / 60);
            uptime -= m*60;
            let res = '';
            if (d) {
                res += `${d} day${((d > 1) ? 's' : '')} `;
            }
            res += `${pad2(h)}:${pad2(m)}:${pad2(uptime)}`;
            $('#lbUptime').html(res);
        } else if (payload.colormap) {
            // console.info('Got new colormap', payload.colormap);
            setColormap(payload.colormap);
        } else if (payload.image) {
            // console.info('Got new image', payload.image);
            setImage(payload.image);
        } else if (payload.sequence) {
            console.info('Got new sequence', payload.sequence);
            setSequence(payload.sequence);
        } else if ((payload.command || payload.uiCommand || payload.vuiCommand) && payload.result) {
            if (payload.command) {
                console.log(`[C] ${payload.command}:`, payload.result);
            } else if (payload.uiCommand) {
                console.log(`[U] ${payload.uiCommand}:`, payload.result);
            } else {
                console.log(`[V] ${payload.vuiCommand}:`, payload.result);
            }

            if (!dispatchCommandResult(payload)) {
                if (payload.command) {
                    console.error(`[C] Nothing to do for ${payload.command} ?`);
                } if (payload.uiCommand) {
                    console.error(`[U] Nothing to do for ${payload.uiCommand} ?`);
                } else {
                    console.error(`[V] Nothing to do for ${payload.vuiCommand} ?`);
                }
            }
        } else if (payload.newSettings) {
            updateSettings(payload.newSettings);
        } else {
            console.error("payload", payload);
        }
    }


    lbWs.onclose = function(e) {
        $('#lbWebsocketStatus').attr('src', '/ui/static/icons8-wi-fi-disconnected-48.png');
        $('#lbConnecting').show();
        $('#lbShutdown').hide();
        lbWs = null;
        setTimeout(function() {
            connect();
        }, 1000);
    }


    lbWs.onerror = function(err) {
        // console.error('Socket encountered error: ', err, 'Closing socket');
        lbWs.close();
    }
}


function command(cmd) {
    // console.log("SEND", ({ "command": cmd }));
    lbWs.send(JSON.stringify({ "command": cmd }));
}


function uiCommand(cmd, arg = null, noReply = false) {
    // console.log("SEND", ({ uiCommand: cmd, arg: arg, noReply: noReply }));
    lbWs.send(JSON.stringify({ uiCommand: cmd, arg: arg, noReply: noReply }));
}


function start() {
    console.log("Starting Le Biniou");
    $('#lbWebsocketStatus').attr('src', '/ui/static/icons8-wi-fi-disconnected-48.png');
    $('#lbShutdown').hide();
    setupPreferences();
    threedInit();
    initHostPort();
    connect();
    previewConnect();
    pluginsInit();
    sequenceInit();
}


function dispatchCommandResult(r) {
    let res = 0;

    res |= lebiniouCommandResult(r);
    res |= banksCommandResult(r);
    res |= colormapsCommandResult(r);
    res |= imagesCommandResult(r);
    res |= pluginCommandResult(r);
    res |= pluginsCommandResult(r);
    res |= sequenceCommandResult(r);
    res |= threedCommandResult(r);
    res |= shortcutsCommandResult(r);

    return res;
}


function randomModeResult(_what, bool) {
    const what = _what[0].toUpperCase() + _what.substring(1);
    if (bool) {
        $(`#lbRandom${what}`).css({ "color": "black", "background-color": "aqua" });
    } else {
        $(`#lbRandom${what}`).css({ "color": "white", "background-color": "black" });
    }
}


function autoModeResult(_what, bool) {
    const what = _what[0].toUpperCase() + _what.substring(1);
    if (bool) {
        $(`#lbAuto${what}`).css({ "color": "black", "background-color": "aqua" });
    } else {
        $(`#lbAuto${what}`).css({ "color": "white", "background-color": "black" });
    }
}


function lebiniouCommandResult(r) {
    // console.log("lebiniouCommandResult", r);
    switch (r.command) {
    case "CMD_APP_CLEAR_SCREEN":
    case "CMD_APP_NEXT_WEBCAM":
    case "CMD_APP_RANDOMIZE_SCREEN":
    case "CMD_APP_QUIT":
    case "CMD_APP_SAVE_QUIT":
    case "CMD_APP_SWITCH_BYPASS":
    case "CMD_APP_DISPLAY_COLORMAP":
        return 1;

    case "CMD_APP_FREEZE_INPUT":
        muteResult(r.result.mute);
        return 1;

    case "CMD_APP_NEXT_RANDOM_MODE":
    case "CMD_APP_PREVIOUS_RANDOM_MODE":
        randomModeResult("schemes", r.result.randomSchemes);
        randomModeResult("sequences", r.result.randomSequences);
        return 1;

    case "CMD_APP_SET_WEBCAM_REFERENCE":
        return 1;
        
    case "CMD_APP_STOP_AUTO_MODES":
        randomModeResult("schemes", false);
        randomModeResult("sequences", false);
        autoModeResult("colormaps", false);
        autoModeResult("images", false);
        return 1;

    case "CMD_APP_SWITCH_CURSOR":
        return 1;

    case "CMD_APP_VOLUME_SCALE_DOWN":
    case "CMD_APP_VOLUME_SCALE_UP":
        $('#lbVolumeScale').html(double_value(r.result.volumeScale * 1000));
        return 1;

    case "CMD_APP_SWITCH_FULLSCREEN":
        setFullscreen(r.result.fullscreen);
        return 1;

    case "CMD_APP_TOGGLE_AUTO_COLORMAPS":
        autoModeResult("colormaps", r.result.autoColormaps);
        return 1;

    case "CMD_APP_TOGGLE_AUTO_IMAGES":
        autoModeResult("images", r.result.autoImages);
        return 1;

    case "CMD_SEQ_RESET":
        $('#lbSequence').empty();
        return 1;
    }

    switch (r.uiCommand) {
    case "UI_CMD_CONNECT":
        $('#lbConnecting').hide();
        $('#lbWebsocketStatus').attr('src', '/ui/static/icons8-wi-fi-48.png');
        if (!r.result.inputPlugin) {
            $('#lbVolumeScaleLabel').hide();
            $('#lbVolumeScaleContainer').hide();
            $('#lbMuteLabel').hide();
            $('#lbMuteContainer').hide();
        }
        $('#lbShutdown').show();
        const {
            version, ulfius, sequenceName, sequence, selectedPluginDname,
            randomSchemes, randomSequences, autoColormaps, autoImages,
            autoColormapsMode, autoImagesMode, autoSequencesMode,
            colormapsMin, colormapsMax, imagesMin, imagesMax, sequencesMin, sequencesMax,
            webcams, fadeDelay, volumeScale, maxFps, mute,
            outputPlugins, width, height,
            bandpassMin, bandpassMax, fullscreen, encoding
        } = r.result;
        screenWidth = width;
        screenHeight = height;
        console.info(`Screen resolution: ${screenWidth}x${screenHeight}`);
        $('#lbVersion').html(version);
        $('#lbUlfiusVersion').html(ulfius);
        selectedPlugin = r.result.selectedPlugin;
        setSequenceName(sequenceName);
        setSequence(sequence);
        $('#lbSelectedPluginDname').html(selectedPluginDname);
        randomModeResult("schemes", randomSchemes);
        randomModeResult("sequences", randomSequences);
        autoModeResult("colormaps", autoColormaps);
        autoModeResult("images", autoImages);
        fetch(`${lbHttpUrl}/parameters/${selectedPlugin}`).then(result => parameters({ parameters: result }));
        $('#lbFadeDelay').html(`${Math.floor(fadeDelay * 1000)} ms`);
        $('#lbFadeDelaySlider').slider({
            min: 10,
            max: 10000,
            values: [ fadeDelay * 1000 ],
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_FADE_DELAY', ui.value, true);
                $('#lbFadeDelay').html(`${ui.value} ms`);
            }
        });
        $('#lbBandpassRange').html(`${bandpassMin} - ${bandpassMax}`);
        $('#lbBandpassSlider').slider({
            range: true,
            min: 0,
            max: 255,
            values: [ bandpassMin, bandpassMax ],
            slide: function(event, ui) {
                const min = ui.values[0];
                const max = ui.values[1];
                uiCommand('UI_CMD_APP_SET_BANDPASS', { min: min, max: max }, true);
                $('#lbBandpassRange').html(`${min} - ${max}`);
            }
        });
        $('#lbVolumeScale').html(doubleValue(volumeScale * 1000));
        $('#lbVolumeScaleSlider').slider({
            min: 10,
            max: 10000,
            values: [ volumeScale * 1000 ],
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_VOLUME_SCALE', ui.value, true);
                $('#lbVolumeScale').html(doubleValue(ui.value));
            }
        });
        $('#lbAutoColormapsRange').html(`${colormapsMin} - ${colormapsMax}`);
        $('#lbAutoColormapsSlider').slider({
            range: true,
            min: 1,
            max: 60,
            values: [ colormapsMin, colormapsMax ],
            slide: function(event, ui) {
                const min = ui.values[0];
                const max = ui.values[1];
                uiCommand('UI_CMD_APP_SET_DELAY', { what: "colormaps", min: min, max: max }, true);
                $('#lbAutoColormapsRange').html(`${min} - ${max}`);
            }
        });
        $('#lbAutoImagesRange').html(`${imagesMin} - ${imagesMax}`);
        $('#lbAutoImagesSlider').slider({
            range: true,
            min: 1,
            max: 60,
            values: [ imagesMin, imagesMax ],
            slide: function(event, ui) {
                const min = ui.values[0];
                const max = ui.values[1];
                uiCommand('UI_CMD_APP_SET_DELAY', { what: "images", min: min, max: max }, true);
                $('#lbAutoImagesRange').html(`${min} - ${max}`);
            }
        });
        $('#lbAutoSequencesRange').html(`${sequencesMin} - ${sequencesMax}`);
        $('#lbAutoSequencesSlider').slider({
            range: true,
            min: 1,
            max: 60,
            values: [ sequencesMin, sequencesMax ],
            slide: function(event, ui) {
                const min = ui.values[0];
                const max = ui.values[1];
                uiCommand('UI_CMD_APP_SET_DELAY', { what: "sequences", min: min, max: max }, true);
                $('#lbAutoSequencesRange').html(`${min} - ${max}`);
            }
        });
        if (!webcams) {
            $('#lbBypassWebcam').hide();
            $('#lbPluginsTabWebcam').hide();
        }
        if (webcams > 1) {
            const { autoWebcamsMode, webcamsMin, webcamsMax } = r.result;
            $('#lbAutoWebcamsMode').val(autoWebcamsMode);
            $('#lbAutoWebcamsRange').html(`${webcamsMin} - ${webcamsMax}`);
            $('#lbAutoWebcamsSlider').slider({
                range: true,
                min: 1,
                max: 60,
                values: [ webcamsMin, webcamsMax ],
                slide: function(event, ui) {
                    const min = ui.values[0];
                    const max = ui.values[1];
                    uiCommand('UI_CMD_APP_SET_DELAY', { what: "webcams", min: min, max: max }, true);
                    $('#lbAutoWebcamsRange').html(`${min} - ${max}`);
                }
            });
        } else {
            $('#lbAutoWebcamsRow').hide();
        }
        $('#lbMaxFps').html(`Max: ${maxFps}`);
        $('#lbMaxFpsSlider').slider({
            min: 1,
            max: 255,
            values: [ maxFps ],
            slide: function(event, ui) {
                uiCommand('UI_CMD_APP_SET_MAX_FPS', ui.value, true);
                $('#lbMaxFps').html(`Max: ${ui.value}`);
            }
        });

        if (outputPlugins) {
            if (outputPlugins.length > 1) {
                $('#lbOutputsLabel').html("Outputs");
            }
            if (!outputPlugins.includes("SDL2")) {
                $('#lbOutputSdl2').hide();
            } else {
                setFullscreen(fullscreen);
            }
            if (!outputPlugins.includes("mp4")) {
                $('#lbOutputMp4').hide();
            } else {
                setEncoding(encoding);
            }
        } else {
            $('#lbOutputsLabel').hide();
            $('#lbOutputsRow').hide();
        }
        $('#lbAutoSequencesMode').val(autoSequencesMode);
        $('#lbAutoColormapsMode').val(autoColormapsMode);
        $('#lbAutoImagesMode').val(autoImagesMode);
        muteResult(mute);
        layerModes = r.result.layerModes;
        const { colormaps, images } = r.result.shortcuts;
        createShortcuts('colormap', 'COL', colormaps);
        createShortcuts('image', 'IMG', images);
        $('#lbConnecting').hide();
        return 1;

    case "UI_CMD_APP_SET_AUTO_MODE":
        return 1;

    case "UI_CMD_APP_SELECT_PLUGIN":
        $('#lbSelectedPluginDname').html(r.result.selectedPluginDname);
        pluginsInit();
        return 1;

    case "UI_CMD_APP_SET_BANDPASS":
        $('#lbBandpassRange').html(`${r.result[0]} - ${r.result[1]}`);
        $('#lbBandpassSlider').slider({ values: [ r.result[0], r.result[1] ] });
        return 1;

    case "UI_CMD_APP_SET_DELAY":
        const { what, range } = r.result;
        const [ min, max ] = range;
        switch (what) {
        case "colormaps":
            $('#lbAutoColormapsRange').html(`${min} - ${max}`);
            $('#lbAutoColormapsSlider').slider({ values: [ min, max ] });
            break;

        case "images":
            $('#lbAutoImagesRange').html(`${min} - ${max}`);
            $('#lbAutoImagesSlider').slider({ values: [ min, max ] });
            break;

        case "sequences":
            $('#lbAutoSequencesRange').html(`${min} - ${max}`);
            $('#lbAutoSequencesSlider').slider({ values: [ min, max ] });
            break;

        case "webcams":
            $('#lbAutoWebcamsRange').html(`${min} - ${max}`);
            $('#lbAutoWebcamsSlider').slider({ values: [ min, max ] });
            break;
        }
        return 1;

    case "UI_CMD_APP_SET_FADE_DELAY":
        $('#lbFadeDelay').html(`${Math.floor(r.result.fadeDelay)} ms`);
        $('#lbFadeDelaySlider').slider({ values: [ r.result.fadeDelay ] });
        return 1;

    case "UI_CMD_APP_TOGGLE_RANDOM_SCHEMES":
        randomModeResult("schemes", r.result.randomSchemes);
        return 1;

    case "UI_CMD_APP_TOGGLE_RANDOM_SEQUENCES":
        randomModeResult("sequences", r.result.randomSequences);
        return 1;

    case "UI_CMD_APP_SET_MAX_FPS":
        $('#lbMaxFps').html("Max: " + r.result.maxFps);
        $('#lbMaxFpsSlider').slider({ values: [ r.result.maxFps ] });
        return 1;

    case "UI_CMD_APP_SET_VOLUME_SCALE":
        $('#lbVolumeScale').html(doubleValue(r.result.volumeScale));
        return 1;

    case "UI_CMD_NOOP":
        return 1;

    case "UI_CMD_OUTPUT":
        outputResult(r.result);
        return 1;
    }

    return 0;
}


function initHostPort() {
    $('#lbHost').val(localStorage.getItem("lbHost") || "localhost");
    $('#lbPort').val(localStorage.getItem("lbPort") || 30543);
}


function setHostPort() {
    const host = $('#lbHost').val();
    const port = $('#lbPort').val();

    lbHttpUrl = `http://${host}:${port}`;
    lbWsUrl = `ws://${host}:${port}/ui`;
    lbPreviewUrl = `ws://${host}:${port}/ui/preview`;

    localStorage.setItem("lbHost", host);
    localStorage.setItem("lbPort", +port);

    if (lbWs) {
        lbWs.close();
    }
    if (lbPreviewWs) {
        lbPreviewWs.close();
    }
}

window.onunload = function() {
    if (lbWs) {
        lbWs.close();
    }
}


function pad2(x) {
    return (x < 10) ? '0'+ x : x;
}


function switchBooleanPreference(name, pref) {
    lbPrefs[pref] = !lbPrefs[pref];
    localStorage.setItem(name, lbPrefs[pref]);
}


function setVisible(e, b) {
    if (b) {
        e.show();
    } else {
        e.hide();
    }
}


function toggle(what) {
    switch (what) {
    case "lbPrefDisplayColormap":
        switchBooleanPreference("lbPrefDisplayColormap", "displayColormap");
        setVisible($('#lbColormapRow'), lbPrefs.displayColormap);
        break;

    case "lbPrefDisplayImage":
        switchBooleanPreference("lbPrefDisplayImage", "displayImage");
        setVisible($('#lbImageRow'), lbPrefs.displayImage);
        break;

    case "lbPrefDisplayFrame":
        switchBooleanPreference("lbPrefDisplayFrame", "displayFrame");
        setVisible($('#lbFrameContainer'), lbPrefs.displayFrame);
        if (lbPrefs.displayFrame) {
            if (lbPreviewTimeout !== null) {
                console.error('toggle frame display: lbPreviewTimeout is not null !', lbPreviewTimeout);
                clearTimeout(lbPreviewTimeout);
                lbPreviewTimeout = null;
            }
            updateFrame();
        } else {
            clearTimeout(lbPreviewTimeout);
            lbPreviewTimeout = null;
        }
        break;

    case "lbPrefDisplayThreed":
        switchBooleanPreference("lbPrefDisplayThreed", "displayThreed");
        setVisible($('#lbThreed'), lbPrefs.displayThreed);
        if (lbPrefs.displayThreed) {
            webGLStart();
        }
        break;

    case "lbPrefDisplayOutputs":
        switchBooleanPreference("lbPrefDisplayOutputs", "displayOutputs");
        setVisible($('#lbOutputs'), lbPrefs.displayOutputs);
        break;

    case "lbPrefDisplayTooltips":
        switchBooleanPreference("lbPrefDisplayTooltips", "displayTooltips");
        if (lbPrefs.displayTooltips) {
            $(document).tooltip("enable");
        } else {
            $(document).tooltip("disable");
        }
        break;

    case "lbPrefDisplayControls":
        switchBooleanPreference("lbPrefDisplayControls", "displayControls");
        if (lbPrefs.displayControls) {
            $(".lb-sidebar").show();
            $(".lb-plugins").css({ "grid-column": "span 7" });
        } else {
            $(".lb-sidebar").hide();
            $(".lb-plugins").css({ "grid-column": "span 12" });
        }
        break;
    }
}


function setupPreferences() {
    lbPrefs.displayColormap = JSON.parse(localStorage.getItem("lbPrefDisplayColormap"));
    if (lbPrefs.displayColormap === null) lbPrefs.displayColormap = true;
    setVisible($('#lbColormapRow'), lbPrefs.displayColormap);

    lbPrefs.displayImage = JSON.parse(localStorage.getItem("lbPrefDisplayImage"));
    if (lbPrefs.displayImage === null) lbPrefs.displayImage = true;
    setVisible($('#lbImageRow'), lbPrefs.displayImage);

    lbPrefs.displayFrame = JSON.parse(localStorage.getItem("lbPrefDisplayFrame"));
    if (lbPrefs.displayFrame === null) lbPrefs.displayFrame = true;
    lbPrefs.previewFps = JSON.parse(localStorage.getItem("lbPrefPreviewFps"));
    if (lbPrefs.previewFps === null) lbPrefs.previewFps = 1;
    lbPreviewFps = lbPrefs.previewFps;
    setVisible($('#lbFrameContainer'), lbPrefs.displayFrame);
    if (lbPrefs.displayFrame) {
        $('#lbFrameRefresh').html(`${lbPreviewFps} fps`);
    }

    lbPrefs.displayThreed = JSON.parse(localStorage.getItem("lbPrefDisplayThreed"));
    if (lbPrefs.displayThreed === null) lbPrefs.displayThreed = true;
    setVisible($('#lbThreed'), lbPrefs.displayThreed);

    lbPrefs.displayOutputs = JSON.parse(localStorage.getItem("lbPrefDisplayOutputs"));
    if (lbPrefs.displayOutputs === null) lbPrefs.displayOutputs = true;
    setVisible($('#lbOutputs'), lbPrefs.displayOutputs);

    lbPrefs.displayTooltips = JSON.parse(localStorage.getItem("lbPrefDisplayTooltips"));
    if (lbPrefs.displayTooltips === null) lbPrefs.displayTooltips = true;
    $('#lbTooltips').prop("checked", lbPrefs.displayTooltips);
    $(document).tooltip();
    if (lbPrefs.displayTooltips) {
        $(document).tooltip("enable");
    } else {
        $(document).tooltip("disable");
    }

    lbPrefs.displayControls = JSON.parse(localStorage.getItem("lbPrefDisplayControls"));
    if (lbPrefs.displayControls === null) lbPrefs.displayControls = true;
    $('#lbControls').prop("checked", lbPrefs.displayControls);
}


function doubleValue(d) {
    return (d / 1000).toFixed(2);
}


function clearFrame() {
    if (window.event.shiftKey) {
        command('CMD_APP_RANDOMIZE_SCREEN');
    } else {
        command('CMD_APP_CLEAR_SCREEN');
    }
}


function setFullscreen(b) {
    if (b) {
        $('#lbFullscreen').attr("src", "/ui/static/icons8-normal-screen-24.png");
        $('#lbFullscreen').attr("title", "Unset fullscreen");
    } else {
        $('#lbFullscreen').attr("src", "/ui/static/icons8-toggle-full-screen-24.png");
        $('#lbFullscreen').attr("title", "Set fullscreen");
    }
}


function setEncoding(b) {
    if (b) {
        $('#lbEncoding').attr("src", "/ui/static/icons8-stop-squared-24.png");
        $('#lbEncodingButton').attr("onclick", "uiCommand('UI_CMD_OUTPUT', { plugin: 'mp4', command: 'stop_encoding' });");
        $('#lbEncodingButton').attr("title", "Stop video encoding");
    } else {
        $('#lbEncoding').attr("src", "/ui/static/icons8-record-24.png");
        $('#lbEncodingButton').attr("onclick", "uiCommand('UI_CMD_OUTPUT', { plugin: 'mp4', command: 'start_encoding' });");
        $('#lbEncodingButton').attr("title", "Start video encoding");
    }
}


function outputResult(r) {
    switch (r.plugin) {
    case "mp4":
        setEcoding(r.result.encoding);
        break;
    }
}


function shutdown() {
    if (lbWs) {
        if (confirm("Quit Le Biniou ?")) {
            $('#lbWebsocketStatus').attr('src', '/ui/static/icons8-wi-fi-disconnected-48.png');
            command('CMD_APP_QUIT');
        }
    }
}


function muteResult(b) {
    if (b) {
        $('#lbMuteButtonImg').attr("src", "/ui/static/icons8-play-button-circled-30.png");
        $('#lbMute').html("Unfreeze input");
        $('#lbMuteButton').attr("title", "Unfreeze input");
    } else {
        $('#lbMuteButtonImg').attr("src", "/ui/static/icons8-pause-button-30.png");
        $('#lbMute').html("Freeze input");
        $('#lbMuteButton').attr("title", "Freeze input");
    }
}


function updateSettings(settings) {
    // console.log('updateSettings', settings);
    const { engine, input } = settings;
    const {
        autoColormapsMode, autoImagesMode, autoSequencesMode, autoWebcamsMode,
        colormapsMin, colormapsMax,
        imagesMin, imagesMax,
        sequencesMin, sequencesMax,
        webcamsMin, webcamsMax,
        fadeDelay, maxFps, randomMode
    } = engine
    // Random modes
    $('#lbAutoColormapsMode').val(autoColormapsMode);
    $('#lbAutoImagesMode').val(autoImagesMode);
    $('#lbAutoSequencesMode').val(autoSequencesMode);
    $('#lbAutoWebcamsMode').val(autoWebcamsMode);
    // Random delays
    $('#lbAutoColormapsRange').html(`${colormapsMin} - ${colormapsMax}`);
    $('#lbAutoColormapsSlider').slider({ values: [ colormapsMin, colormapsMax ] });
    $('#lbAutoImagesRange').html(`${imagesMin} - ${imagesMax}`);
    $('#lbAutoImagesSlider').slider({ values: [ imagesMin, imagesMax ] });
    $('#lbAutoSequencesRange').html(`${sequencesMin} - ${sequencesMax}`);
    $('#lbAutoSequencesSlider').slider({ values: [ sequencesMin, sequencesMax ] });
    $('#lbAutoWebcamsRange').html(`${webcamsMin} - ${webcamsMax}`);
    $('#lbAutoWebcamsSlider').slider({ values: [ webcamsMin, webcamsMax ] });
    // Misc
    $('#lbMaxFps').html(`Max: ${maxFps}`);
    $('#lbMaxFpsSlider').slider({ values: [ maxFps ] });
    $('#lbFadeDelay').html(`${Math.floor(fadeDelay * 1000)} ms`);
    $('#lbFadeDelaySlider').slider({ values: [ fadeDelay * 1000 ] });
    // Auto modes
    switch (randomMode) {
    case 0:
        randomModeResult("schemes", false);
        randomModeResult("sequences", false);
        break;

    case 1:
        randomModeResult("schemes", false);
        randomModeResult("sequences", true);
        break;

    case 2:
        randomModeResult("schemes", true);
        randomModeResult("sequences", false);
        break;

    case 3:
        randomModeResult("schemes", true);
        randomModeResult("sequences", true);
        break;
    }
    // Input
    const { volumeScale } = input;
    $('#lbVolumeScale').html(doubleValue(volumeScale * 1000));
    $('#lbVolumeScaleSlider').slider({ values: [ volumeScale * 1000 ] });
}
