/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";


const DELAY = 2000; // for clear/store bank CSS changes
const MAX_BANKS = 24;
let bank, bankSet, banks = new Array(MAX_BANKS);


const bankSetKeyOff = (k) => {
    $(`#lbBankSet${k}`).css({
        "background-color": "black",
        "color": "lime"
    });
}


const bankSetKeyOn = (k) => {
    $(`#lbBankSet${k}`).css({
        "background-color": "orange",
        "color": "black"
    });
}


const bankKeyOff = (k) => {
    $(`#lbBank${k}`).css({
        "background-color": "black",
        "color": banks[k - 1] ? "orange" : "lime"
    });
}


const bankKeysOff = () => {
    for (let i = 1; i <= MAX_BANKS; i++) {
        bankKeyOff(i);
    }
}


const bankSetChange = (b) => {
    command(`CMD_APP_USE_BANK_SET_${b}`);
    bankSet = b;
}


const bankKeyOn = (b) => {
    $(`#lbBank${b}`).css({
        "background-color": "orange",
        "color": "black"
    });
}


const bankChange = (b) => {
    uiCommand('UI_CMD_BANK', { "command": "use", "bank": b });
    bankKeyOn(b);
}


const bankStore = (b) => {
    // console.log("Bank store: " + b);
    uiCommand('UI_CMD_BANK', { "command": "store", "bank": b });
    $(`#lbBank${b}`).css({
        "background-color": "lime",
        "color": "black"
    });
    setTimeout(function() {
        if (b != bank) {
            $(`#lbBank${b}`).css({
                "background-color": "black",
                "color": "orange"
            });
        } else {
            bankKeyOn(b);
        }
    }, DELAY);
}


const bankClear = (b) => {
    // console.log("Bank clear: " + b);
    if (confirm(`Clear bank ${b} ?`)) {
        // bankKeysOff();
        $(`#lbBank${b}`).css({
            "background-color": "red",
            "color": "black"
        });
        setTimeout(function() {
            uiCommand('UI_CMD_BANK', { "command": "clear", "bank": b });
            $(`#lbBank${b}`).css({
                "background-color": "black",
                "color": "lime"
            });
        }, DELAY);
    }
}


const bankClick = (b) => {
    if (window.event.ctrlKey && window.event.shiftKey) {
        bankClear(b);
    } else if (window.event.shiftKey) {
        bankStore(b);
    } else {
        bankChange(b);
    }
}


const banksInit = () => {
    // console.log("Initializing banks");
    $('#lbBankSets').empty();
    $('#lbBankSets').append('<div class="lb-label lb-bank-sets-label" title="Select a bank set">Set</div>');
    $('#lbBanks').empty();
    $('#lbBanks').append('<div class="lb-label lb-banks-label" title="Click to use a bank, Shift-click to store, Ctrl-Shift-click to erase">Bank</div>');

    for (let i = 1; i <= MAX_BANKS; ++i) {
        $('#lbBankSets').append(`<div class="lb-bank"><button id="lbBankSet${i}" onclick="bankSetChange(${i});" class="lb-button lb-bank-button">${i}</button></div>`);
        $('#lbBanks').append(`<div class="lb-bank"><button id="lbBank${i}" onclick="bankClick(${i});" class="lb-button lb-bank-button">${i}</button></div>`);
    }
    bankSetKeyOn(bankSet);
    bankKeyOn(bank);
}


const updateBanks = () => {
    for (const i in banks) {
        const bankNo = +i + 1;

        if (banks[i]) {
            $(`#lbBank${bankNo}`).css({ 'color': 'orange' });
            $(`#lbBank${bankNo}`).attr('title', banks[i]);
        } else {
            $(`#lbBank${bankNo}`).css({ 'color': 'lime' });
            $(`#lbBank${bankNo}`).removeAttr('title');
        }
        if (bank === bankNo) {
            $(`#lbBank${bankNo}`).css({ 'color': 'black', 'background-color': 'orange' });
        } else {
            $(`#lbBank${bankNo}`).css({ 'background-color': 'black' });
        }
    }
}


const banksCommandResult = (r) => {
    if (r.command) {
        if (r.command.startsWith("CMD_APP_USE_BANK_SET_")) {
            bankKeyOff(bank);
            bankSet = r.command.replace(/^CMD_APP_USE_BANK_SET_/, '');
            for (let i = 1; i <= MAX_BANKS; i++) {
                bankSetKeyOff(i);
            }
            bankSetKeyOn(bankSet);
            bank = 1;
            bankKeyOn(bank);
            banks = r.result;
            updateBanks();
            return 1;
        }

        if (r.command.startsWith("CMD_APP_CLEAR_BANK_")) {
            const cleared = r.command.replace(/^CMD_APP_CLEAR_BANK_/, '');
            banks[cleared - 1] = null;
            updateBanks();
            return 1;
        }

        if (r.command.startsWith("CMD_APP_STORE_BANK_")) {
            banks = r.result;
            updateBanks();
            return 1;
        }

        if (r.command.startsWith("CMD_APP_USE_BANK_")) {
            bankKeyOff(bank);
            bank = r.command.replace(/^CMD_APP_USE_BANK_/, '');
            bankKeyOn(bank);
            return 1;
        }
    }

    if (r.uiCommand) {
        switch (r.uiCommand) {
        case "UI_CMD_CONNECT":
            bankSet = r.result.bankSet + 1;
            bank = r.result.bank + 1;
            banksInit();
            bankKeyOn(bank);
            banks = r.result.banks;
            updateBanks();
            return 1;

        case "UI_CMD_BANK":
            switch (r.result.command) {
            case "clear":
                banks[r.result.bank - 1] = null;
                $(`#lbBank${r.result.bank}̀`).removeAttr('title');
                return 1;

            case "store":
                banks[r.result.bank - 1] = r.result.sequenceName;
                $(`#lbBank${r.result.bank}`).attr('title', r.result.sequenceName);
                $('#lbSequenceName').html(r.result.sequenceName);
                return 1;

            case "use":
                bankKeyOff(bank);
                bank = r.result.bank;
                bankKeyOn(bank);
                return 1;
            }
        }
    }

    return 0;
}
