/*
 *  Copyright 1994-2021 Olivier Girondel
 *
 *  This file is part of lebiniou.
 *
 *  lebiniou is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  lebiniou is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with lebiniou. If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let plugins = [], plugin = "", demos = [];
let player;
const BASE_URL = "https://media.biniou.net";

let options = {
    "BO_BLUR": false,
    "BO_COLORMAP": false,
    "BO_DISPLACE": false,
    "BO_GFX": false,
    "BO_IMAGE": false,
    "BO_LENS": false,
    "BO_MIRROR": false,
    "BO_ROLL": false,
    "BO_SCROLL": false,
    "BO_SFX": false,
    "BO_SPLASH": false,
    "BO_WARP": false,
    "BO_WEBCAM": false,
    "BO_NORANDOM": false,
    "BO_UNIQUE": false,
    "BO_FIRST": false,
    "BO_LAST": false,
    "BO_HOR": false,
    "BO_VER": false
}


function start() {
    $(document).tooltip();
    $(document).tooltip("enable");
    $('#lbSequence').hide();
    $('#lbVideoSelect').hide();
    player = videojs('lbVideo');
    videojs('lbVideo', { autoplay: true });
    player.play();
    let myVideo = document.getElementById('lbVideo');
    if (typeof myVideo.loop == 'boolean') { // loop supported
        myVideo.loop = true;
    } else { // loop property not supported
        myVideo.addEventListener('ended', function() {
            this.currentTime = 0;
            this.play();
        }, false);
    }
    fetchPlugins().then(result => {
        plugins = result.sort((a, b) => a.displayName > b.displayName);
        buildOptions();
        buildList();
    });
}


function fetchPlugins() {
    return fetch('/plugins', {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        }
    }).then(response => response.json());
}


function buildOptions() {
    for (const i in options) {
        const name = i.replace("BO_", "");
        const div = `<div><button id="lbOption_${i}" class="lb-option" onclick="addOption('${i}');">${name}</button></div>`;
        $('#lbOptions').append(div);
    }
}


function addOption(option) {
    if (window.event.ctrlKey) {
        options[option] = !options[option];
    } else {
        for (const o in options) {
            options[o] = false;
        }
        options[option] = true;
    }
    for (const o in options) {
        let el = $(`#lbOption_${o}`);
        options[o] ? el.removeClass('lb-unselected-option') : el.removeClass('lb-selected-option');
        options[o] ? el.addClass('lb-selected-option') : el.addClass('lb-unselected-option');
    }
    refreshPlugins().then(result => {
        plugins = result;
        buildList();
    });
}


function refreshPlugins() {
    return fetch('/plugins', {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(options)
    }).then(response => response.json());
}


function buildList() {
    $('#lbPlugins').empty();
    for (const i in plugins) {
        const { name, displayName, favorite } = plugins[i];
        const src = favorite ? "icons8-pin-on-20.png" : "1x1.png";
        const div = `<div class="lb-plugin-container"><img id="lbFavorite_${name}" src="/ui/static/${src}" width="20" height="20" class="lb-favorite" onclick="switchFavorite(${i}, '${name}', '${displayName}');" /><button id="lbPlugin_${i}" class="lb-plugin" onclick="search('${name}');">${displayName}</button></div>`;
        $('#lbPlugins').append(div);
        $(`#lbPlugin_${i}`).hover(function() { favorite ? displayPin(name, true) : displayPin(name, false); }, function() { favorite ? displayPin(name, true) : favoritePixel(name); });
        $(`#lbFavorite_${name}`).hover(function() { displayPin(name, false); }, function() { favorite ? displayPin(name, true) : favoritePixel(name); });
    }
}


function displayPin(name, status) {
    $(`#lbFavorite_${name}`).attr("src", `/ui/static/icons8-pin-${status ? 'on' : 'off'}-20.png`);
}


function favoritePixel(name) {
    $(`#lbFavorite_${name}`).attr("src", `/ui/static/1x1.png`);
}


function search(p) {
    plugin = p;
    $('#lbSequence').hide();
    demos = [];
    fetch(`https://media.biniou.net/red/demos/plugins?name=${plugin}`).then(response => response.json()).then(result => {
        demos = result;
        if (demos.length) {
            $('#lbMessage').hide();
            $('#lbVideoSelect').show()
            buildVideoSelect();
            playVideo(0);
        } else {
            $('#lbVideoSelect').hide();
            $('#lbMessage').show();
            playDefaultVideo();
        }
    });
}


function buildVideoSelect() {
    $('#lbVideoSelect').empty();
    for (const i in demos) {
        const demo = demos[i];
        const option = `<option value="${i}">${demo.option.split('.')[0]}</option>`;
        $('#lbVideoSelect').append(option);
    }
}


function playDefaultVideo() {
    player.src({ src: '/videos/code-loop.mp4', type: 'video/mp4' });
    player.ready(function() { player.play(); });
}

function playVideo(index) {
    const { sequence, file } = demos[index];
    const src = `${BASE_URL}/videos/${file}`;

    if (sequence) {
        displaySequence(sequence);
    }
    player.src({ src: src, type: 'video/mp4' });
    player.ready(function () {
        player.play();
    });
}


function displaySequence(sequence) {
    const {
        autoColormaps, autoImages,
        colormap, image,
        plugins
    } = sequence;
    $('#lbColormapName').html(colormap);
    $('#lbImageName').html(image.split('.')[0]);
    $('#lbAutoColormaps').html(onOff(autoColormaps));
    $('#lbAutoImages').html(onOff(autoImages));
    displayPlugins(plugins);
    $('#lbSequence').show();
}


function onOff(bool) {
    return bool ? "On" : "Off";
}


function displayPlugins(plugins) {
    let html = '<ol>';
    for (const plugin in plugins) {
        const {
            name, displayName,
            mode, lens, parameters
        } = plugins[plugin];
        let li = `<li><div class="lb-plugin-name"><button onclick="search('${name}');">${displayName}</button></div>`;
        if (parameters) {
            li += '<ul>';
            for (const param in parameters) {
                li += `<li><span class="lb-param">${param}:</span>&nbsp;<span class="lb-param-value">${JSON.stringify(parameters[param].value)}</span></li>`;
            }
            li += '</ul>';
        }
        li += '</li>';
        html += li;
    }
    html += '</ol>';
    $('#lbSequencePlugins').html(html);
}


function switchFavorite(i, plugin, displayName) {
    fetch(`/settings.html?favorite=${plugin}`, {
        method: 'post',
        headers: {
            "Content-type": "application/json"
        },
        body: JSON.stringify(null)
    }).then(response => response.json()).then(data => {
        const { name, favorite } = data;
        $(`#lbFavorite_${name}`).attr('src', `/ui/static/icons8-pin-${favorite ? 'on' : 'off'}-20.png`);
        $(`#lbPlugin_${i}`).hover(function() { favorite ? displayPin(name, true) : displayPin(name, false); }, function() { favorite ? displayPin(name, true) : favoritePixel(name); });
        const src = `${favorite ? '/ui/static/icons8-pin-on-20.png' : '/ui/static/1x1.png'}`;
        if (favorite) {
            $('#lbFavorite').html(`${displayName} added to the favorites`);
            $(`#lbFavorite_${name}`).attr('src', src);
            $(`#lbFavorite_${name}`).hover(function() { displayPin(name, true); }, function() { favorite ? displayPin(name, true) : favoritePixel(name); });
        } else {
            $('#lbFavorite').html(`${displayName} removed from the favorites`);
            $(`#lbFavorite_${name}`).attr('src', src);
            $(`#lbFavorite_${name}`).hover(function() { displayPin(name, favorite); }, function() { favorite ? displayPin(name, true) : favoritePixel(name); });
        }
        setTimeout(function() {
            $('#lbFavorite').empty();
        }, 3000);
    });
}
