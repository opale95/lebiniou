<!DOCTYPE html>
<html>
  <head>
    <title>Le Biniou - settings</title>

    <meta charset="UTF-8">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="/ui/static/w3.mincss">
    <link rel="stylesheet" type="text/css" href="/ui/static/vanillaSelectBox.mincss">
    <link rel="stylesheet" type="text/css" href="/ui/static/settings.mincss">

    <script type="text/javascript" src="jquery.min.js"></script>
    <script type="text/javascript" src="jquery-ui.min.js"></script>
    <script type="text/javascript" src="/ui/static/vanillaSelectBox.minjs"></script>
    <script type="text/javascript" src="/ui/static/settings.minjs"></script>
  </head>

  <body>
    <div class="container">
      <h1 style="text-align: center;">Settings</h1>
      <div>The configuration will be written to <span id="lbConfigurationFile" class="formatted"></span>.</div>
      <div>You can use different configurations by using the <span class="lb-code w3-round">-c</span> or <span class="lb-code w3-round">--config</span> command-line option. Example: <span class="lb-code w3-round">$ <span id="lbCommand"></span> --config my_config.json</span></div>
      <br />
      <b class="lb-setting">Save settings</b>&nbsp;<button id="lbSaveSettings" onclick="postForm();" title="Save settings"><img src="/ui/static/icons8-save-48.png" /></button>
      <div id="lbSymlink">
        <br />
        Create link on the desktop:&nbsp;<input type="checkbox" id="lbCreateSymlink" name="createSymlink" />
      </div>
      <input type="hidden" id="lbVersion" name="version" />
      <input type="hidden" id="lbFlatpak" name="flatpak" />
      <hr />
      <div id='lbThemesContainer'></div>
      <div id="lbPlugins">
      </div>
      <br />
      <div class="input-container">
        <div id="lbInput"></div>
        <div class="lb-jackaudio-container">
          <div><b class="lb-setting">Left:&nbsp;</b></div>
          <div><input id="lbJackaudioLeft" name="jackaudioLeft" type="text" /></div>
          <div><b class="lb-setting">&nbsp;Right:&nbsp;</b></div>
          <div><input id="lbJackaudioRight" name="jackaudioRight" type="text" /></div>
        </div>
        <br />
        <b class="lb-setting">Volume scale:&nbsp;</b><input id="lbVolumeScale" name="volumeScale" type="number" step="0.1" min="0.1" />
      </div>
      <br />
      <div class="screen-container">
        <div id='lbResolution'></div>
        <br />
        <b class="lb-setting">Width:&nbsp;</b><input id="lbWidthInput" name="width" type="number" min="80" onchange="setResolution();" />&nbsp;
        <b class="lb-setting">Height:&nbsp;</b><input id="lbHeightInput" name="height" type="number" min="60" onchange="setResolution();" />
        <br />
        <br />
      </div>
      <b class="lb-setting">Frames per second:&nbsp;</b><input type="number" id="lbMaxFps" name="maxFps" min="1" max="255" />
      <br />
      <br />
      <b class="lb-setting">Fade delay:&nbsp;</b><input type="number" id="lbFadeDelay" name="fadeDelay" min="1" max="255" />&nbsp;Fading duration for colormaps and images.
      <br />
      <br />
      <div><b class="lb-setting">Start sequence:&nbsp;</b>
        <select id="lbStartMode" name="startMode">
          <option value="first_created">First created</option>
          <option value="last_created">Last created</option>
          <option value="last_updated">Last updated</option>
        </select>
      </div>
      <br>
      <div>
        <b class="lb-setting">Random mode:</b>
        <select id="lbRandomMode" name="randomMode">
          <option value="0">None</option>
          <option value="1">Sequences</option>
          <option value="2">Schemes</option>
          <option value="3">Sequences and schemes</option>
        </select>
      </div>
      <br>
      <div>
        <b class="lb-setting">Sequences:</b>
        <select id="lbAutoSequencesMode" name="autoSequencesMode">
          <option value="cycle">Cycle</option>
          <option value="shuffle">Shuffle</option>
          <option value="random">Random</option>
        </select>&nbsp;between
        <input type="number" id="lbSequencesMin" name="sequencesMin" min="1" max="60" />&nbsp;and
        <input type="number" id="lbSequencesMax" name="sequencesMax" min="1" max="60" />&nbsp;seconds.
      </div>
      <br>
      <div>
        <b class="lb-setting">Colormaps:</b>
        <select id="lbAutoColormapsMode" name="autoColormapsMode">
          <option value="cycle">Cycle</option>
          <option value="shuffle">Shuffle</option>
          <option value="random">Random</option>
        </select>&nbsp;between
        <input type="number" id="lbColormapsMin" name="colormapsMin" min="1" max="60" />&nbsp;and
        <input type="number" id="lbColormapsMax" name="colormapsMax" min="1" max="60" />&nbsp;seconds.
      </div>
      <br />
      <div>
        <b class="lb-setting">Images:</b>
        <select id="lbAutoImagesMode" name="autoImagesMode">
          <option value="cycle">Cycle</option>
          <option value="shuffle">Shuffle</option>
          <option value="random">Random</option>
        </select>&nbsp;between
        <input type="number" id="lbImagesMin" name="imagesMin" min="1" max="60" />&nbsp;and
        <input type="number" id="lbImagesMax" name="imagesMax" min="1" max="60" />&nbsp;seconds.
      </div>
      <br />
      <b class="lb-setting">Webcams:&nbsp;</b><input type="number" id="lbWebcams" name="webcams" min="0" />&nbsp;
      <b class="lb-setting">Flip webcam:</b>
      Horizontally&nbsp;<input type="checkbox" id="lbHFlip" name="hFlip" />
      Vertically&nbsp;<input type="checkbox" id="lbVFlip" name="vFlip" />
      <br />
      <br />
      <div id="lbAutoWebcamsRow">
        <div>
          <b class="lb-setting">Webcams</b>
          <select id="lbAutoWebcamsMode" name="autoWebcamsMode">
            <option value="cycle">Cycle</option>
            <option value="shuffle">Shuffle</option>
            <option value="random">Random</option>
          </select>&nbsp;between
          <input type="number" id="lbWebcamsMin" name="webcamsMin" min="1" max="60" />&nbsp;and
          <input type="number" id="lbWebcamsMax" name="webcamsMax" min="1" max="60" />&nbsp;seconds.
        </div>
        <br />
      </div>
      <b class="lb-setting">Statistics:&nbsp;</b>Help us improve Le Biniou by sending anonymous usage statistics.&nbsp;<input type="checkbox" id="lbStatistics" name="statistics" />
    </div>
    <script>$(document).ready(() => start());</script>
  </body>
</html>
